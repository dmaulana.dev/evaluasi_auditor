<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionDetails;

class RoleSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        Role::truncate();
        PermissionDetails::truncate();
        $role = new Role;
        $role->name = 'superadmin';
        $role->description = 'superadmin';
        $role->created_by = 'seeder';
        $role->updated_by = 'seeder';
        $role->save();

        $permissions = Permission::get();
        foreach ($permissions as $key => $value) {
            $permDetails = new PermissionDetails;
            $permDetails->role_id = $role->id;
            $permDetails->role_name = $role->name;
            $permDetails->permission_id = $value['id'];
            $permDetails->visible = 1;
            $permDetails->save();
        }
    }
}
