<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        Permission::truncate();

        $permissions = [
            [
                "name" => "Dashboard",
                "description" => null,
                "url_name" => "dashboard",
                "url_path" => "dashboard",
                "icon" => "menu-icon tf-icons mdi mdi-home-outline",
                "order_number" => 1,
                "children" => [
                    [
                        "name" => "Dashboard",
                        "description" => "dashboard for admin",
                        "url_name" => "dashboard.admin",
                        "url_path" => "dashboard-admin",
                        "icon" => null,
                        "order_number" => 1,
                    ]
                ]
            ],
            [
                "name" => "Master Data",
                "description" => null,
                "url_name" => "master-data",
                "url_path" => "master-data",
                "icon" => "menu-icon tf-icons mdi mdi-window-maximize",
                "order_number" => 2,
                "children" => [
                    [
                        "name" => "Admin",
                        "description" => null,
                        "url_name" => "master-admin.index",
                        "url_path" => "master-admin",
                        "icon" => null,
                        "order_number" => 1,
                    ],
                    [
                        "name" => "User",
                        "description" => null,
                        "url_name" => "master-user.index",
                        "url_path" => "master-user",
                        "icon" => null,
                        "order_number" => 2,
                    ]
                ]
            ],
            [
                "name" => "Transaction",
                "description" => null,
                "url_name" => "transaction",
                "url_path" => "transaction",
                "icon" => "menu-icon tf-icons mdi mdi-window-maximize",
                "order_number" => 1,
                "created_by" => "seeder",
                "updated_by" => "seeder"
            ],
            [
                "name" => "Settings",
                "description" => null,
                "url_name" => "setting",
                "url_path" => "setting",
                "icon" => "mdi mdi-cog-outline me-2",
                "order_number" => 3,
                "children" => [
                    [
                        "name" => "Configuration",
                        "description" => null,
                        "url_name" => "configuration.index",
                        "url_path" => "configuration",
                        "icon" => null,
                        "order_number" => 1,
                    ],
                    [
                        "name" => "Permission",
                        "description" => null,
                        "url_name" => "permission.index",
                        "url_path" => "permission",
                        "icon" => null,
                        "order_number" => 2,
                    ],
                    [
                        "name" => "Role",
                        "description" => null,
                        "url_name" => "role.index",
                        "url_path" => "role",
                        "icon" => null,
                        "order_number" => 3,
                    ]
                ]
            ],
        ];

        foreach (collect($permissions) as $permission) {
            $data = new Permission;
            $data->name = $permission['name'];
            $data->description = $permission['description'];
            $data->url_name = $permission['url_name'];
            $data->url_path = $permission['url_path'];
            $data->icon = $permission['icon'];
            $data->parent_id = null;
            $data->parent_name = null;
            $data->order_number = $permission['order_number'];
            $data->created_by = 'seeder';
            $data->updated_by = 'seeder';
            $data->save();

            if (!empty($permission['children'])) {
                foreach ($permission['children'] as $child) {
                    $data2 = new Permission;
                    $data2->name = $child['name'];
                    $data2->description = $child['description'];
                    $data2->url_name = $child['url_name'];
                    $data2->url_path = $child['url_path'];
                    $data2->icon = $child['icon'];
                    $data2->parent_id = $data->id;
                    $data2->parent_name = $data->name;
                    $data2->order_number = $data->order_number;
                    $data2->created_by = 'seeder';
                    $data2->updated_by = 'seeder';
                    $data2->save();
                }
            }
        }
    }
}
