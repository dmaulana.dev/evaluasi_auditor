<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailPertanyaanPelatihanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pertanyaan_pelatihan', function (Blueprint $table) {
            $table->id();
            $table->integer('auditor_pelatihan_id');
            $table->integer('pertanyaan_id');
            $table->string('answer');
            $table->string('catatan');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pertanyaan_pelatihan');
    }
}
