<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefPertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_pertanyaan', function (Blueprint $table) {
            $table->id();
            $table->integer('kategori');
            $table->string('tahun');
            $table->string('pertanyaan');
            $table->string('jenis_jawaban');
            $table->string('pilihan');
            $table->integer('is_catatan');
            $table->integer('is_upload');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_pertanyaan');
    }
}
