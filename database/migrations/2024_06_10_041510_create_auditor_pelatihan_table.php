<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditorPelatihanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditor_pelatihan', function (Blueprint $table) {
            $table->id();
            $table->integer('pelatihan_id');
            $table->integer('auditor_id');
            $table->string('tahun');
            $table->integer('status');
            $table->integer('verifikasi');
            $table->string('catatan');
            $table->integer('aktif');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditor_pelatihan');
    }
}
