<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailHistoriPelatihanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_histori_pelatihan', function (Blueprint $table) {
            $table->id();
            $table->integer('auditor_pelatihan_id');
            $table->string('nama_pelatihan');
            $table->string('tahun_pelatihan');
            $table->integer('aktif');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_histori_pelatihan');
    }
}
