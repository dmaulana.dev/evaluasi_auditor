<!-- Add Role Modal -->
<div class="modal fade" id="addRoleModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-add-new-role">
        <div class="modal-content p-3 p-md-5">
            <form method="POST" id="submit">
                <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal"
                aria-label="Close"></button>
                <div class="modal-body p-md-0">
                    <div class="text-center mb-4">
                        <h3 class="role-title mb-2 pb-0">Buat Role Baru</h3>
                        <p>Set izin role menu</p>
                    </div>
                    <div class="col-12 mb-4">
                        <div class="form-floating form-floating-outline mb-3">
                            <input type="text" id="modalRoleName" name="modalRoleName" class="form-control"
                            placeholder="Masukkan Nama Role" tabindex="-1" />
                            <label for="modalRoleName">Nama Role</label>
                        </div>
                        <div class="form-floating form-floating-outline">
                            <input type="text" id="modalRoleDesc" name="modalRoleDesc" class="form-control"
                            placeholder="Masukkan Keterangan" tabindex="-1" />
                            <label for="modalRoleDesc">Keterangan</label>
                        </div>
                    </div>
                    <div class="col-12">
                        <h5>Pengaturan Izin Role / Menu</h5>
                        <!-- Permission table -->
                        {{-- {{dd($permission)}} --}}
                        <div class="table-responsive">
                            <table class="table table-flush-spacing">
                                <tbody>
                                    @for ($i = 0; $i < count($permission); $i++)
                                    {{-- Parent --}}
                                    @if ($permission[$i]->parent_id == null)
                                    <tr>
                                        <td class="text-nowrap fw-semibold">{{ $permission[$i]->name }}
                                        </td>
                                        <td>
                                            <div class="d-flex">
                                                <div class="form-check me-3 me-lg-5">
                                                    <input class="form-check-input parent-input"
                                                    type="checkbox" value="{{ $permission[$i]->id }}"
                                                    id="parent-select{{ $permission[$i]->id }}"
                                                    name="permission" />
                                                    <label class="form-check-label" for="parent-select{{ $permission[$i]->id }}">
                                                        {{ $permission[$i]->name }}
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                    
                                    {{-- Child --}}
                                    @if ($permission[$i]->parent_id != null)
                                    <tr>
                                        <td class="text-nowrap fw-semibold"></td>
                                        <td class="ps-5">
                                            <div class="d-flex">
                                                <div class="form-check me-3 me-lg-5">
                                                    <input class="form-check-input child-input parent-select{{ $permission[$i]->parent_id }}"
                                                    type="checkbox"
                                                    id="child-select{{ $permission[$i]->id }}"
                                                    data-parent_id="parent-select{{ $permission[$i]->parent_id }}"
                                                    value="{{ $permission[$i]->id }}"
                                                    name="permission" />
                                                    <label class="form-check-label"
                                                    for="child-select{{ $permission[$i]->id }}">
                                                    {{ $permission[$i]->name }}</label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                        <!-- Permission table -->
                    </div>
                    <div class="col-12 text-center">
                        <button type="submit" id="btnSubmit"
                        class="btn btn-primary btn-submit me-sm-3 me-1">Submit</button>
                        <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal"
                        aria-label="Close">Cancel</button>
                    </div>
                    <!--/ Add role form -->
                </div>
            </form>
        </div>
    </div>
</div>
<!--/ Add Role Modal -->

@push('custom-scripts')
<script>
    $(document).ready(function(){
        $('#addRoleModal .parent-input').on('click', function(){
            const id = $(this).attr('id');
            const checked = $(this).prop('checked');
            
            $(`#addRoleModal .${id}`).each((idx) => {
                $($(`#addRoleModal .${id}`)[idx]).prop('checked', checked);
            })
        })
        
        $('#addRoleModal .child-input').on('click', function(){
            const parentID = $(this).data('parent_id');
            const checked = $(this).prop('checked');
            if($(`#addRoleModal #${parentID}`).prop('checked') === false){
                $(`#addRoleModal #${parentID}`).prop('checked', checked);
            }
        })
    })
</script>

<script>
    $("#addRoleModal #submit").on('submit', function(e) {
        e.preventDefault();
        showLoading();
        
        var roleName = $("#modalRoleName").val();
        var roleDesc = $("#modalRoleDesc").val();
        
        var rolePicked = $("input[name='permission']:checked").map(function(_, el) {
            return {
                id: $(el).val(),
                allow: true
            }
        }).get();
        
        var jsonPicked = JSON.stringify(rolePicked);
        
        $.ajax({
            url: "{{ route('role.store') }}",
            method: "POST",
            dataType: "JSON",
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                "roleName": roleName,
                "roleDesc": roleDesc,
                "permission": jsonPicked
            },
            success: function(response) {
                $("#addRoleModal").modal('hide');
                if (response.type == "success") {
                    $('#submit')[0].reset();
                    $('#addRoleModal').modal('toggle')
                    Swal.fire({
                        type: 'success',
                        title: 'Tambah Role Berhasil!',
                        showConfirmButton: true
                    });
                } else {
                    Swal.fire({
                        type: 'info',
                        title: 'Tambah role gagal!',
                        text: "Silahkan coba lagi!",
                        showCancelButton: true,
                    });
                }
            },
            error: function(jqXhr, json, errorThrown) {
                $("#addRoleModal").modal('hide');
                Swal.fire({
                    type: 'error',
                    title: 'Tambah role gagal!',
                    text: "Silahkan coba lagi!",
                    showCancelButton: true,
                    showConfirmButton: false
                });
            }
        });
    });
</script>

@endpush