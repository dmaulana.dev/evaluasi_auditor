@extends('layouts.admin._settings')
@section('content')
<div class="content-wrapper">
    <div class="container-xxl flex-grow-1 container-p-y">
        <!-- Role cards -->
        <div class="row g-4">
            <h4 class="fw-medium mb-1 mt-5">Pengaturan Role</h4>
            <p class="mb-0 mt-1">Temukan role kalian di kolom tabel dibawah ini!</p>
            
            <div class="col-12">
                <!-- Role Table -->
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-10">
                                <h5 class="card-title">Setting Role</h5>
                            </div>
                            <div class="col-md-2">
                                <a type="button" class="btn-sm btn btn-warning" data-bs-toggle = "modal" data-bs-target = "#addRoleModal">
                                    <i class="mdi mdi-plus me-0 me-sm-1"></i>
                                    <span class="d-none d-sm-inline-block">Tambah Role</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table class="table" id="dataTables">
                            <thead class="table-light">
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Keterangan</th>
                                    <th>Created By</th>
                                    <th>Updated BY</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!--/ Role Table -->
            </div>
        </div>
        <!--/ Role cards -->
        
        @include('settings.role.addModal')
        <div class="modal fade" id="editRoleModal" tabindex="-1" aria-hidden="true">
            {{-- @include('settings.role.editModal') --}}
        </div>
        
    </div>
</div>


@push('custom-scripts')
@include('settings.role.javascript')
<script async defer src="https://buttons.github.io/buttons.js"></script>
@endpush
@endsection
