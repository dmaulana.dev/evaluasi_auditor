{{-- @extends('layouts.admin._master-admin') --}}
@extends('layouts.admin._settings')
@section('content')
<div class="content-wrapper">
    <!-- Content -->
    
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-semibold pt-2 mb-1">Table Permission</h4>
        
        <p class="mb-4">
            Beri izin pada setiap role yang terdaftar pada kolom dibawah ini!
        </p>
        
        <!-- Permission Table -->
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-10">
                        <h5 class="card-title">Setting Permission / Menu</h5>
                    </div>
                    <div class="col-md-2">
                        <a type="button" class="btn-sm btn btn-warning addPermissionModal"> <i class="mdi mdi-plus me-0 me-sm-1"></i>
                            <span class="d-none d-sm-inline-block">Tambah Permission</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-datatable table-responsive">
                <table class="table" id="dataTables">
                    <thead class="table-light">
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Nama URL</th>
                            <th>Tanggal</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!--/ Permission Table -->
        
        <div class="content-backdrop fade"></div>
        
        <div class="modal fade" id="PermissionModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" id="modalContent">
                
            </div>
        </div>
        
    </div>
    
    
    @push('custom-scripts')
    @include('settings.permission.javascript')
    
    
    {{-- <script>
        $("#btn-submit").click(function() {
        var namePermission = $("#name_permission").val();
        var description = $("#description").val();
        var url_name = $("#url_name").val();
        var urlPath = $("#url_path").val();
        var icon = $("#icon").val();
        var parentId = $("#parent_id").val();
        var orderNumber = $("#order_number").val();
        
        $.ajax({
        url: "{{ route('permission.store') }}",
        type: "POST",
        dataType: "JSON",
        cache: false,
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
        "name": namePermission,
        "description": description,
        "url_name": url_name,
        "url_path": urlPath,
        "icon": icon,
        "parent_id": parentId,
        "order_number": orderNumber
        },
        success: function(response) {
        if (response.type == "success") {
        $("#addPermissionModal").modal('hide');
        Swal.fire({
        type: 'success',
        title: 'Tambah Permission Berhasil!',
        showConfirmButton: true
        });
        } else {
        $("#addPermissionModal").modal('hide');
        Swal.fire({
        type: 'info',
        title: 'Tambah Permission Gagal!',
        text: "Silahkan coba lagi!",
        showCancelButton: true,
        });
        }
        },
        error: function(jqXhr, json, errorThrown) {
        $("#addPermissionModal").modal('hide');
        Swal.fire({
        type: 'error',
        title: 'Tambah Permission Gagal!',
        text: "Silahkan coba lagi!",
        showCancelButton: true,
        showConfirmButton: false
        });
        }
        });
        });
    </script> --}}
    
    @endpush
    
    @endsection
    
    