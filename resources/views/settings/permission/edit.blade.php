<div class="modal-content p-3 p-md-5" id="createAkunAdminForm">
    <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal" aria-label="Close"></button>
    <div class="modal-body p-md-0">
        <div class="text-center mb-4">
            <h3 class="mb-2 pb-1">Edit Permission / Menu</h3>
            <p>Edit Permission / Menu</p>
        </div>
        
        
        <form action="{{route('permission.update', MainSett::encodeId($data->id))}}" method="POST">
            @csrf
            <div class="form-floating form-floating-outline">
                <input type="text" id="name" name="name" class="form-control"  value="{{$data->name}}"/>
                <label>Nama Permission</label>
            </div>
            <div class="form-floating form-floating-outline mt-3">
                <input type="text" id="urlPath" name="url_path" class="form-control" value="{{$data->url_path}}"/>
                <label>URL Path</label>
            </div>
            
            <div class="form-floating form-floating-outline mt-3">
                <input type="text" id="urlName" name="url_name" class="form-control" value="{{$data->url_name}}"/>
                <label>URL Name</label>
            </div>
            
            <div class="form-floating form-floating-outline mt-3">
                <input type="text" id="icons" name="icon" class="form-control" value="{{$data->icon}}"/>
                <label>Icon</label>
            </div>
            
            <div class="form-floating form-floating-outline mt-3">
                <input type="number" id="orderNumber" name="order_number" class="form-control" value="{{$data->order_number}}"/>
                <label>Order Number</label>
            </div>
            
            <div class="form-floating form-floating-outline mt-3">
                <select class="form-control" name="parent_id">
                    <option id="parentName" value=""></option>
                    @foreach ($list_permission as $item)
                    <option {{ $data->parent_name === $item->name ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
                <label>Parent Name</label>
            </div>
            
            <div class="col-12 text-center demo-vertical-spacing">
                <button type="submit" class="btn btn-primary me-sm-3 me-1" id="btn-submit">Simpan</button>
                <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">Tutup</button>
            </div>
        </form>
        
    </div>
</div>