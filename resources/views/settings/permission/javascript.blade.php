<script>
    $(document).ready(function() {
        
        let dataTableBasic = $("#dataTables"),
        dt_basic;
        
        // Users List datatable
        if (dataTableBasic.length) {
            dt_basic = dataTableBasic.DataTable({
                ajax: {
                    url: `{{ route('permission.index') }}`,
                },
                processing: true,
                serverSide: true,
                columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: "name",
                    render: function(_, __, data) {
                        return data['name'];
                    },
                },
                {
                    data: "url_path",
                    render: function(_, __, data) {
                        return data['url_path'];
                    },
                },
                {
                    data: "created_at",
                    render: function(_, __, data) {
                        return data['created_at'];
                    },
                },
                {
                    data: "action",
                },
                ],
                order: [
                [0, "asc"]
                ],
                dom: '<"row mx-1"' +
                '<"col-sm-12 col-md-3" l>' +
                '<"col-sm-12 col-md-9"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1"<"me-3"f>B>>' +
                ">t" +
                '<"row mx-2"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                ">",
                language: {
                    sLengthMenu: "Show _MENU_",
                    search: "Search",
                    searchPlaceholder: "Search..",
                },
                buttons: []
            });
        }
        
        $("body").on("click", ".addPermissionModal", function() {
            const id = $(this).data("id");
            showLoading();
            $.ajax({
                url: `{{ route('permission.create') }}`,
                method: 'GET',
                success: (response) => {
                    Swal.close();
                    $('#modalContent').html(response);
                    $('#PermissionModal').modal('toggle')
                },
                error: () => {
                    Swal.fire({
                        type: 'error',
                        text: 'Silahkan Coba Lagi!',
                        timer: 3000,
                    })
                }
            })
        });
        
        
        //show permission
        $("body").on("click", ".btn-edit", function() {
            const id = $(this).data("id");
            showLoading();
            const _id = `{{ MainSett::generateRandomString(20) }}${bin2hex(id)}`;
            $.ajax({
                url: `{{ route('permission.index') }}/edit/${_id}`,
                type: "GET",
                cache: false,
                success: function(response) {
                    Swal.close();
                    console.log('res',response)
                    $('#modalContent').html(response);
                    $('#PermissionModal').modal('toggle')
                },
                error: function(jqXhr, json, errorThrown) {
                    $("#PermissionModal").modal('hide');
                    console.log(errorThrown);
                }
            });
        });
        
        
        $("body").on("click", ".btn-delete", function () {
            var id = $(this).data("id");
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
                preConfirm: function() {
                    return new Promise(function(resolve) {
                        const _id = `{{ MainSett::generateRandomString(20) }}${bin2hex(id)}`;
                        $.ajax({
                            url: `{{route('permission.index')}}/destroy/${_id}`,
                            type: "GET",
                            success: function(response) {
                                if (response.type == "success") {
                                    Swal.fire({
                                        type: 'success',
                                        title: 'Deleted Success',
                                        showConfirmButton: true
                                    });
                                    location.reload();
                                } else {
                                    Swal.fire({
                                        type: 'info',
                                        title: 'Failed!',
                                        text: "Deleted Not Success",
                                        showCancelButton: true,
                                    });
                                    location.reload();
                                }
                            },
                        });
                    });
                },
            })
        });
        
    })
</script>
