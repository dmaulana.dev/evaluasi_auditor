<div class="modal-content p-3 p-md-5" id="createAkunAdminForm">
    <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal" aria-label="Close"></button>
    <div class="modal-body p-md-0">
        <div class="text-center mb-4">
            <h3 class="mb-2 pb-1">Tambah Permission / Menu</h3>
            <p>Tambah Permission / Menu</p>
        </div>
        
        <form action="{{ route('permission.store')}}" method="POST">
            @csrf
            <div class="col-12 mb-3">
                <div class="form-floating form-floating-outline">
                    <input type="text" id="name_permission" name="name" class="form-control" placeholder="Name Persmission" />
                    <label>Name Permission</label>
                </div>
                <div class="form-floating form-floating-outline mt-3">
                    <textarea type="text" id="description" name="description" class="form-control" placeholder="description">
                    </textarea>
                    <label>Description</label>
                </div>
                
                <div class="form-floating form-floating-outline mt-3">
                    <input type="text" id="url_path" name="url_path" class="form-control"
                    placeholder="dashboard-admin" />
                    <label>URL Path</label>
                </div>
                
                <div class="form-floating form-floating-outline mt-3">
                    <input type="text" id="url_name" name="url_name" class="form-control"
                    placeholder="admin.dashboard" />
                    <label>URL Name</label>
                </div>
                
                <div class="form-floating form-floating-outline mt-3">
                    <input type="text" id="icon" name="icon" class="form-control"
                    placeholder="Icon" />
                    <label>Icon</label>
                </div>
                
                <div class="form-floating form-floating-outline mt-3">
                    <input type="number" id="order_number" name="order_number" class="form-control"
                    placeholder="order number" />
                    <label>Order Number</label>
                </div>
                
                <div class="form-floating form-floating-outline mt-3">
                    <select class="form-control" id="parent_id" name="parent_id">
                        <option id="parentName" value=""></option>
                        @foreach ($list_permission as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                    <label>Parent Name</label>
                </div>
                
                <div class="col-12 text-center demo-vertical-spacing">
                    <button type="submit" class="btn btn-primary me-sm-3 me-1" id="btn-submit">Simpan</button>
                    <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">Tutup</button>
                </div>
            </div>
        </form>
    </div>
</div>
