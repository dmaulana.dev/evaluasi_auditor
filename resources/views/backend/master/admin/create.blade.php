<div class="modal-content p-3 p-md-5" id="createAkunAdminForm">
    <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal" aria-label="Close"></button>
    <div class="modal-body p-md-0">
        <div class="text-center mb-4">
            <h3 class="mb-2 pb-1">Tambah Akun Admin</h3>
            <p>Tambah Akun Admin</p>
        </div>
        
        <form action="{{route('master-admin.store')}}" method="POST">
            @csrf
            <div class="col-12 mb-3">
                <div class="form-floating form-floating-outline">
                    <input type="text" id="nama_lengkap" name="nama_lengkap"
                    class="form-control" placeholder="Nama Lengkap"/>
                    <label>Nama Lengkap</label>
                </div>
            </div>
            
            <div class="form-floating form-floating-outline mt-3">
                <input type="text" id="nomor_identitas" name="nomor_identitas" class="form-control" placeholder="326xxxx"/>
                <label>Nomor Identitas</label>
            </div>
            
            <div class="form-floating form-floating-outline mt-3">
                <input type="email" id="email" name="email" class="form-control" placeholder="lasernarindo@gmail.com"/>
                <label>Email</label>
            </div>
            
            <div class="form-floating form-floating-outline mt-3">
                <input type="text" id="no_hp" name="no_hp" class="form-control" placeholder="081387xx26xx"/>
                <label>No Handphone</label>
            </div>
            
            <div class="form-floating form-floating-outline mt-3">
                <select class="form-control" id="role" name="role">
                    @foreach ($list_role as $item)
                    <option value="{{ $item->id }}">{{ $item->name}}</option>
                    @endforeach
                </select>
                <label>Role Name</label>
            </div>
            
            <div class="col-12 text-center demo-vertical-spacing">
                <button type="submit" class="btn btn-primary btn-submit me-sm-3 me-1">Simpan</button>
                <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                    Tutup
                </button>
            </div>
        </form>
        
    </div>
</div>
