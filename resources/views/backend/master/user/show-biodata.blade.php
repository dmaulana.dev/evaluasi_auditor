@extends('layouts.admin._master-admin')
@section('content')

<!-- Content -->
<div class="content-wrapper">
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Account Settings /</span> Account</h4>

        <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-pills flex-column flex-md-row mb-3 gap-2 gap-lg-0">
            <li class="nav-item">
                <a class="nav-link active" href="javascript:void(0);"
                ><i class="mdi mdi-account-outline mdi-20px me-1"></i>Account</a
                >
            </li>
            <li class="nav-item">
                <a class="nav-link" href="pages-account-settings-security.html"
                ><i class="mdi mdi-lock-open-outline mdi-20px me-1"></i>Security</a
                >
            </li>
           
            </ul>
            <div class="card mb-4">
            <h4 class="card-header">Profile Detail {{ $list_profil->nama_lengkap }}</h4>
            <!-- Account -->
            <div class="card-body">
                <div class="d-flex align-items-start align-items-sm-center gap-4">
                <img
                    src='../../../public/admin/assets/img/avatars/{{ $list_profil->photo }}'
                    alt="user-avatar"
                    class="d-block w-px-120 h-px-120 rounded"
                    id="uploadedAvatar" />
                <div class="button-wrapper">
                    <label for="avatar" class="btn btn-primary me-2 mb-3" tabindex="0">
                    <span class="d-none d-sm-block">Upload new photo</span>
                    <i class="mdi mdi-tray-arrow-up d-block d-sm-none"></i>
                    <input
                        type="file"
                        id="avatar"
                        class="account-file-input"
                        hidden
                        accept="image/png, image/jpeg" />
                    </label>

                    <div class="text-muted small">Allowed JPG or PNG. Max size of 500K</div>
                </div>
                </div>
            </div>
            <div class="card-body pt-2 mt-1">
                <form id="formBiodata" method="POST" enctype="multipart/form-data">
                <div class="row mt-2 gy-4">
                    
                    <div class="col-md-12">
                        <label for="ijazah" class="form-label">Upload Ijazah Terakhir</label>
                        <input class="form-control" type="file" id="ijazah" name="ijazah" />
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating form-floating-outline">
                            <select id="pendidikan" class="select2 form-select">
                            <option value="">Pilih Pendidikan Terakhir</option>
                            <option value="S3">Strata 3</option>
                            <option value="S2">Strata 2</option>
                            <option value="S1">Strata 1</option>
                            <option value="D4">Diploma 4</option>
                            <option value="D3">Diploma 3</option>
                            </select>
                            <label for="pendidikan">Pendidikan Terakhir</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating form-floating-outline">
                            <input
                            class="form-control"
                            type="text"
                            id="namalengkap"
                            name="namalengkap"
                            value="{{$list_profil->nama_lengkap}}"
                            autofocus />
                            <label for="namalengkap">Nama Lengkap</label>
                            <input
                            type="text"
                            id="id_profil"
                            name="id_profil"
                            hidden
                            value="{{$list_profil->id}}"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-floating form-floating-outline">
                        <input
                        class="form-control"
                        type="text"
                        id="email"
                        name="email"
                        value="{{$list_profil->email}}"
                        placeholder="test@gmail.com" />
                        <label for="email">E-mail</label>
                    </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group input-group-merge">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    id="ponsel"
                                    name="ponsel"
                                    class="form-control"
                                    placeholder="08xxxxxxx"
                                    value="{{$list_profil->ponsel}}"
                                    onchange="ValidatePonsel()" />
                                <label for="phoneNumber">Phone Number</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating form-floating-outline">
                            <select class="select2 form-select" id="provinsi" name="provinsi" ></select>
                            <label for="provinsi">Provinsi</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating form-floating-outline">
                            <select class="select2 form-select" id="kabupaten" name="kabupaten" ></select>
                            <label for="kabupaten">Kabupaten</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating form-floating-outline">
                            <select class="select2 form-select" id="kecamatan" name="kecamatan" ></select>
                            <label for="kecamatan">Kecamatan</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating form-floating-outline">
                            <select class="select2 form-select" id="kelurahan" name="kelurahan" ></select>
                            <label for="kelurahan">Kelurahan</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group input-group-merge">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    id="nort"
                                    name="nort"
                                    class="form-control"
                                    value="{{$list_profil->rt}}"
                                    onchange="ValidateRt()" />
                                <label for="nort">NO. RT</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group input-group-merge">
                            <div class="form-floating form-floating-outline">
                                <input
                                    type="text"
                                    id="norw"
                                    name="norw"
                                    class="form-control"
                                    value="{{$list_profil->rw}}"
                                    onchange="ValidateRw()" />
                                <label for="norw">NO. RW</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-floating form-floating-outline mb-4">
                            <textarea
                            class="form-control h-px-100"
                            id="alamat"
                            name="alamat"
                            placeholder="Isikan alamat anda disini..."></textarea>
                            <label for="exampleFormControlTextarea1">{{$list_profil->alamat}}</label>
                        </div>
                    </div>
                </div>
                <div class="mt-4">
                    <button type="button" class="btn btn-primary me-2 simpan_biodata">Update</button>
                    <button type="reset" class="btn btn-outline-secondary">Cancel</button>
                </div>
                </form>
            </div>
            </div>
        </div>
        </div>
    </div>
    <div class="content-backdrop fade"></div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
<script>
    $(document).ready(function(){
        $("#kabupaten").attr("disabled", true);
        $("#kecamatan").attr("disabled", true);
        $("#kelurahan").attr("disabled", true);
        $.ajax({
            type: 'GET',
            url: "{{ route('master.provinsi') }}",
            success: function(res){

            $("#provinsi").append('<option value="0">---Pilih Provinsi---</option>');
            $.each(res,function(nama,id){
                $("#provinsi").append('<option value="'+id['id']+'">'+id['nama']+'</option>');
            });
            }
        });
        // OPSI KABUPATEN
        $('#provinsi').change(function(){
            $("#kabupaten").attr("disabled", false);
            var provID = $(this).val(); 
            $.ajax({
                type:"GET",
                url:"{{ route('master.kabupaten') }}",
                data: {id: provID},
                dataType: 'JSON',
                success:function(res){  
                    $("#kabupaten").append('<option value="0">---Pilih Kabupaten---</option>');
                    $.each(res,function(nama,id){
                        $("#kabupaten").append('<option value="'+id['id']+'">'+id['nama']+'</option>');
                    });   
                }
            });   
        });

        //OPSI KECAMATAN
        $('#kabupaten').change(function(){
            $("#kecamatan").attr("disabled", false);
            var kabID = $(this).val(); 
            $.ajax({
                type:"GET",
                url:"{{ route('master.kecamatan') }}",
                data: {id: kabID},
                dataType: 'JSON',
                success:function(res){  
                    $("#kecamatan").append('<option value="0">---Pilih Kecamatan---</option>');
                    $.each(res,function(nama,id){
                        $("#kecamatan").append('<option value="'+id['id']+'">'+id['nama']+'</option>');
                    });   
                }
            });   
        });
        // OPSI KELURAHAN
        $('#kecamatan').change(function(){
            $("#kelurahan").attr("disabled", false);
            var kecID = $(this).val(); 
                $.ajax({
                    type:"GET",
                    url:"{{ route('master.kelurahan') }}",
                    data: {id: kecID},
                    dataType: 'JSON',
                    success:function(res){  
                        $("#kelurahan").append('<option value="0">---Pilih Kelurahan---</option>');
                        $.each(res,function(nama,id){
                            $("#kelurahan").append('<option value="'+id['id']+'">'+id['nama']+'</option>');
                    });   
                }
            });   
        });
    });

    $(".simpan_biodata").click(function(){
        var formData = new FormData();
        let id_profil = $("#id_profil").val();
        var avatar = $('#avatar').prop('files')[0];  
        var ijazah = $('#ijazah').prop('files')[0];
        let pendidikan = $("#pendidikan").val();
        let nama_lengkap = $("#namalengkap").val();
        let email = $("#email").val();
        let ponsel = $("#ponsel").val();
        let provinsi = $("#provinsi").val();
        let kabupaten = $("#kabupaten").val();
        let kecamatan = $("#kecamatan").val();
        let kelurahan = $("#kelurahan").val();
        let nort = $("#nort").val();
        let norw = $("#norw").val();
        let alamat = $("#alamat").val();

        formData.append('id_profil', id_profil);
        formData.append('avatar', avatar);
        formData.append('ijazah', ijazah);
        formData.append('pendidikan', pendidikan);
        formData.append('nama_lengkap', nama_lengkap);
        formData.append('email', email);
        formData.append('ponsel', ponsel);
        formData.append('provinsi', provinsi);
        formData.append('kabupaten', kabupaten);
        formData.append('kecamatan', kecamatan);
        formData.append('kelurahan', kelurahan);
        formData.append('nort', nort);
        formData.append('norw', norw);
        formData.append('alamat', alamat);

        $.ajax({
            contentType: 'multipart/form-data',
            url: "{{ route('biodata.store') }}",
            type: "POST",
            dataType: "JSON",
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response) {
                if (response.type == "success") {
                    Swal.fire({
                        type: response.type,
                        title: response.title,
                        text:response.message,
                        showConfirmButton: true
                    }).then(function (result) {
                        window.location.reload()  
                    });
                    
                } else {
                    Swal.fire({
                        type: response.type,
                        title: response.title,
                        text:response.message,
                        showConfirmButton: true
                    });
                }
            }
            
        });

    });
</script>
<script>
    function ValidatePonsel(){
        var validasiAngka = /^[0-9]+$/;
        var ponsel = $("#ponsel").val();
        var nort = $("#nort").val();
        var norw = $("#norw").val();
        if (ponsel.match(validasiAngka)) {
            return true
        }else{
            Swal.fire({
                type: 'warning',
                title: 'Format Salah',
                text: 'Ponsel hanya boleh angka',
                showConfirmButton: true
            });
            $("#ponsel").val('')
            return false
        }
    }
    function ValidateRt(){
        var validasiAngka = /^[0-9]+$/;
        var nort = $("#nort").val();

        if (nort.match(validasiAngka)) {
            return true
        }else{
            Swal.fire({
                type: 'warning',
                title: 'Format Salah',
                text: 'NO RT hanya boleh angka',
                showConfirmButton: true
            });
            $("#nort").val('')
            return false
        }
    }
    function ValidateRw(){
        var validasiAngka = /^[0-9]+$/;
        var norw = $("#norw").val();

        if (norw.match(validasiAngka)) {
            return true
        }else{
            Swal.fire({
                type: 'warning',
                title: 'Format Salah',
                text: 'NO RW hanya boleh angka',
                showConfirmButton: true
            });
            $("#norw").val('')
            return false
        }
    }
</script>

@push('custom-scripts')
@include('backend.master.user.javascript')
@endpush

@endsection
