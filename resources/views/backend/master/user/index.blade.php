@extends('layouts.admin._master-admin')
@section('content')

<!-- Content -->
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Master /</span> User</h4>
    
    <div class="row g-4 mb-4">
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="avatar">
                            <div class="avatar-initial bg-label-primary rounded">
                                <div class="mdi mdi-account-outline mdi-24px"></div>
                            </div>
                        </div>
                        <div class="ms-3">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-0">8,458</h5>
                                <div class="mdi mdi-chevron-down text-danger mdi-24px"></div>
                                <small class="text-danger">8.1%</small>
                            </div>
                            <small class="text-muted">New Customers</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="avatar">
                            <div class="avatar-initial bg-label-warning rounded">
                                <div class="mdi mdi-poll mdi-24px"></div>
                            </div>
                        </div>
                        <div class="ms-3">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-0">$28.5K</h5>
                                <div class="mdi mdi-chevron-up text-success mdi-24px"></div>
                                <small class="text-success">18.2%</small>
                            </div>
                            <small class="text-muted">Total Profit</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="avatar">
                            <div class="avatar-initial bg-label-info rounded">
                                <div class="mdi mdi-trending-up mdi-24px"></div>
                            </div>
                        </div>
                        <div class="ms-3">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-0">2,450K</h5>
                                <div class="mdi mdi-chevron-down text-danger mdi-24px"></div>
                                <small class="text-danger">24.6%</small>
                            </div>
                            <small class="text-muted">New Transaction</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="avatar">
                            <div class="avatar-initial bg-label-success rounded">
                                <div class="mdi mdi-currency-usd mdi-24px"></div>
                            </div>
                        </div>
                        <div class="ms-3">
                            <div class="d-flex align-items-center">
                                <h5 class="mb-0">$48.2K</h5>
                                <div class="mdi mdi-chevron-down text-success mdi-24px"></div>
                                <small class="text-success">22.5%</small>
                            </div>
                            <small class="text-muted">Total Revenue</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- DataTable with Buttons -->
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-10">
                    <h5 class="card-title">Data Master User</h5>
                </div>
                <div class="col-md-2">
                    <a type="button" class="btn-sm btn btn-warning addUserModal"> <i class="mdi mdi-plus me-0 me-sm-1"></i>
                        <span class="d-none d-sm-inline-block">Tambah User</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="card-datatable table-responsive pt-0">
            <table class="table table-bordered" id="dataTables">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Nomor Identitas</th>
                        <th>Email</th>
                        <th>No HP</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!--/ DataTable with Buttons -->
    
    <div class="modal fade" id="masterUserModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" id="modalContent">
        </div>
    </div>
    
</div>

@push('custom-scripts')
@include('backend.master.user.javascript')
@endpush

@endsection
