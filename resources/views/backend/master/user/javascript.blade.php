<script>
    $(document).ready(function() {
        
        let dataTableBasic = $("#dataTables"),
        dt_basic;
        
        // Users List datatable
        if (dataTableBasic.length) {
            dt_basic = dataTableBasic.DataTable({
                ajax: {
                    url: `{{ route('master-user.index') }}`,
                },
                processing: true,
                serverSide: true,
                columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: "nama_lengkap",
                    render: function(_, __, data) {
                        return data['nama_lengkap'];
                    },
                },
                {
                    data: "nomor_identitas",
                    render: function(_, __, data) {
                        return data['nomor_identitas'];
                    },
                },
                {
                    data: "email",
                    render: function(_, __, data) {
                        return data['email'];
                    },
                },
                {
                    data: "no_hp",
                    render: function(_, __, data) {
                        return data['no_hp'];
                    },
                },
                {
                    data: "active",
                    render: (_, __, data) => {
                        var $status = data.active;
                        statusObj = {
                            0: { title: 'Inactive', class: 'bg-label-danger' },
                            1: { title: 'Active', class: 'bg-label-success' },
                        };
                        return '<span class="badge rounded-pill '
                        + statusObj[$status].class + '" text-capitalized>' + statusObj[$status].title +
                        '</span>'
                    }
                },
                {
                    data: "action",
                },
                ],
                order: [
                [0, "asc"]
                ],
                dom: '<"row mx-1"' +
                '<"col-sm-12 col-md-3" l>' +
                '<"col-sm-12 col-md-9"<"dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1"<"me-3"f>B>>' +
                ">t" +
                '<"row mx-2"' +
                '<"col-sm-12 col-md-6"i>' +
                '<"col-sm-12 col-md-6"p>' +
                ">",
                language: {
                    sLengthMenu: "Show _MENU_",
                    search: "Search",
                    searchPlaceholder: "Search..",
                },
                // dom: 'Bfit',
                //         dom:
                //             "<'row'<'col-sm-6 py-3'B><'col-sm-6 px-3'f>>" +
                //             "<'row'<'col-sm-12 px-3'tr>>" +
                //             "<'row'<'col-sm-4 px-3'i><'col-sm-4 text-center'l><'col-sm-4 px-3'p>>",
                // //         // Buttons with Dropdown
                buttons: []
            });
        }
        
        $("body").on("click", ".addUserModal", function() {
            const id = $(this).data("id");
            showLoading();
            $.ajax({
                url: `{{ route('master-user.create') }}`,
                method: 'GET',
                success: (response) => {
                    Swal.close();
                    $('#modalContent').html(response);
                    $('#masterUserModal').modal('toggle')
                },
                error: () => {
                    Swal.fire({
                        type: 'error',
                        text: 'Silahkan Coba Lagi!',
                        timer: 3000,
                    })
                }
            })
        });
        
        
        //show permission
        $("body").on("click", ".btn-edit", function() {
            const id = $(this).data("id");
            showLoading();
            const _id = `{{ MainSett::generateRandomString(20) }}${bin2hex(id)}`;
            $.ajax({
                url: `{{ route('master-user.index') }}/edit/${_id}`,
                type: "GET",
                cache: false,
                success: function(response) {
                    Swal.close();
                    console.log('res',response)
                    $('#modalContent').html(response);
                    $('#masterUserModal').modal('toggle')
                },
                error: function(jqXhr, json, errorThrown) {
                    $("#masterUserModal").modal('hide');
                    console.log(errorThrown);
                }
            });
        });
        
        
        $("body").on("click", ".btn-delete", function() {
            const id = $(this).data("id");
            const _id = `{{ MainSett::generateRandomString(20) }}${bin2hex(id)}`;
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',
            }).then(({
                dismiss,
                value
            }) => {
                if (value !== undefined) {
                    $.ajax({
                        url: `{{ route('master-user.index') }}/destroy/${_id}`,
                        type: "GET",
                        success: function(response) {
                            if (response.type == "success") {
                                Swal.fire({
                                    type: 'success',
                                    title: 'Deleted Success',
                                    showConfirmButton: true,
                                    timer: 3000,
                                }).then(() => location.reload())
                                
                            } else {
                                Swal.fire({
                                    type: 'error',
                                    title: 'Failed!',
                                    text: "Deleted Not Success",
                                    showCancelButton: true,
                                    timer: 3000,
                                }).then(() => location.reload())
                            }
                        },
                    });
                }
            })
        });
    })
</script>
