<!DOCTYPE html>

<html lang="en" class="light-style layout-navbar-fixed layout-menu-fixed" dir="ltr" data-theme="theme-default"
    data-assets-path="{{URL::asset('public/admin/assets')}}" data-template="vertical-menu-template">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>Dashboard - Administrator | Evaluasi Auditor</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{URL::asset('public/admin/assets/img/favicon/favicon.ico')}}" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap"
        rel="stylesheet" />

    <!-- Icons -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/fonts/materialdesignicons.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/fonts/fontawesome.css')}}" />
    <!-- Menu waves for no-customizer fix -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/node-waves/node-waves.css')}}" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/rtl/core.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/rtl/theme-default.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/demo.css')}}" />

    <!-- Vendors CSS -->
    <link rel="stylesheet"
        href="{{ URL::asset('public/admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/typeahead-js/typeahead.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/apex-charts/apex-charts.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/swiper/swiper.css')}}" />

    <!-- Page CSS -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/pages/cards-statistics.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/pages/cards-analytics.css')}}" />
    <!-- Helpers -->
    <script src="{{ URL::asset('public/admin/assets/vendor/js/helpers.js')}}"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Template customizer: To hide customizer set displayCustomizer value false in config.js.  -->
    <script src="{{ URL::asset('public/admin/assets/vendor/js/template-customizer.js')}}"></script>
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="{{ URL::asset('public/admin/assets/js/config.js')}}"></script>
</head>

<body>
    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">
            <!-- Menu -->
            @include('layouts.admin._menu-admin')
            <!-- / Menu -->

            <!-- Layout container -->
            <div class="layout-page">
                <!-- Navbar -->
                @include('layouts.admin._navbar-admin')
                <!-- / Navbar -->

                <!-- Content wrapper -->
                <div class="content-wrapper">
                    <!-- Content -->

                    <div class="container-xxl flex-grow-1 container-p-y">
                        <div class="row gy-4">
                            <!-- Gamification Card -->
                            <div class="col-md-12 col-lg-12">
                                <div class="card">
                                    <div class="d-flex align-items-end row">
                                        <div class="col-md-6 order-2 order-md-1">
                                            <div class="card-body">
                                                <h4 class="card-title">Selamat Datang, <strong> {{ auth()->user()->nama_lengkap }}</strong>🎉</h4>
                                                <p class="mb-5">Ada Update Apa Hari Ini? 😎</p>
                                                <a href="javascript:;" class="btn btn-primary">Kunjungi Profil</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 text-center text-md-end order-1 order-md-2">
                                            <div class="card-body pb-0 px-0 px-md-4 ps-0">
                                                <img src="{{ URL::asset('public/admin/assets/img/illustrations/illustration-john-light.png') }}"
                                                    height="180" alt="View Profile"
                                                    data-app-light-img="illustrations/illustration-john-light.png"
                                                    data-app-dark-img="illustrations/illustration-john-dark.png" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ Gamification Card -->

                            <!-- Project Statistics -->
                            <div class="col-md-6 col-xl-12">
                                <div class="card">
                                    <div class="card-header d-flex align-items-center justify-content-between">
                                        <h5 class="card-title m-0 me-2">Monitoring Statistik</h5>
                                    </div>
                                    <div class="card-body">
                                        <ul class="p-0 m-0">
                                            <li class="d-flex mb-3 justify-content-between pb-2">
                                                <h6 class="mb-0 small">NAMA</h6>
                                                <h6 class="mb-0 small">TOTAL</h6>
                                            </li>
                                            <li class="d-flex mb-4">
                                                <div class="avatar avatar-md flex-shrink-0 me-3">
                                                    <div class="avatar-initial bg-lighter rounded">
                                                        <div>
                                                            <img src="{{ URL::asset('public/admin/assets/img/icons/misc/3d-illustration.png') }}"
                                                                alt="User" class="h-25" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                                    <div class="me-2">
                                                        <h6 class="mb-0">3D Illustration</h6>
                                                        <small class="text-muted">Blender Illustration</small>
                                                    </div>
                                                    <div class="badge bg-label-primary rounded-pill">$6,500</div>
                                                </div>
                                            </li>
                                            <li class="d-flex mb-4">
                                                <div class="avatar avatar-md flex-shrink-0 me-3">
                                                    <div class="avatar-initial bg-lighter rounded">
                                                        <div>
                                                            <img src="{{ URL::asset('public/admin/assets/img/icons/misc/finance-app-design.png') }}"
                                                                alt="User" class="h-25" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                                    <div class="me-2">
                                                        <h6 class="mb-0">Finance App Design</h6>
                                                        <small class="text-muted">Figma UI Kit</small>
                                                    </div>
                                                    <div class="badge bg-label-primary rounded-pill">$4,290</div>
                                                </div>
                                            </li>
                                            <li class="d-flex mb-4">
                                                <div class="avatar avatar-md flex-shrink-0 me-3">
                                                    <div class="avatar-initial bg-lighter rounded">
                                                        <div>
                                                            <img src="{{ URL::asset('public/admin/assets/img/icons/misc/4-square.png') }}"
                                                                alt="User" class="h-25" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                                    <div class="me-2">
                                                        <h6 class="mb-0">4 Square</h6>
                                                        <small class="text-muted">Android Application</small>
                                                    </div>
                                                    <div class="badge bg-label-primary rounded-pill">$44,500</div>
                                                </div>
                                            </li>
                                            <li class="d-flex mb-4">
                                                <div class="avatar avatar-md flex-shrink-0 me-3">
                                                    <div class="avatar-initial bg-lighter rounded">
                                                        <div>
                                                            <img src="{{ URL::asset('public/admin/assets/img/icons/misc/delta-web-app.png') }}"
                                                                alt="User" class="h-25" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                                    <div class="me-2">
                                                        <h6 class="mb-0">Delta Web App</h6>
                                                        <small class="text-muted">React Dashboard</small>
                                                    </div>
                                                    <div class="badge bg-label-primary rounded-pill">$12,690</div>
                                                </div>
                                            </li>
                                            <li class="d-flex">
                                                <div class="avatar avatar-md flex-shrink-0 me-3">
                                                    <div class="avatar-initial bg-lighter rounded">
                                                        <div>
                                                            <img src="{{ URL::asset('public/admin/assets/img/icons/misc/ecommerce-website.png') }}"
                                                                alt="User" class="h-25" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                                    <div class="me-2">
                                                        <h6 class="mb-0">eCommerce Website</h6>
                                                        <small class="text-muted">Vue + Laravel</small>
                                                    </div>
                                                    <div class="badge bg-label-primary rounded-pill">$10,850</div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--/ Project Statistics -->
                        </div>
                    </div>
                    <!-- / Content -->

                    <!-- Footer -->
                    @include('layouts.admin._footer-admin')
                    <!-- / Footer -->

                    <div class="content-backdrop fade"></div>
                </div>
                <!-- Content wrapper -->
            </div>

            <!-- / Layout page -->
        </div>

        <!-- Overlay -->
        <div class="layout-overlay layout-menu-toggle"></div>

        <!-- Drag Target Area To SlideIn Menu On Small Screens -->
        <div class="drag-target"></div>
    </div>
    <!-- / Layout wrapper -->

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{ URL::asset('public/admin/assets/vendor/libs/jquery/jquery.js')}}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/libs/popper/popper.js')}}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/js/bootstrap.js')}}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/libs/node-waves/node-waves.js')}}"></script>

    <script src="{{ URL::asset('public/admin/assets/vendor/libs/hammer/hammer.js')}}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/libs/i18n/i18n.js')}}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/libs/typeahead-js/typeahead.js')}}"></script>

    <script src="{{ URL::asset('public/admin/assets/vendor/js/menu.js')}}"></script>
    <!-- endbuild -->

    <!-- Vendors JS -->
    <script src="{{ URL::asset('public/admin/assets/vendor/libs/apex-charts/apexcharts.js')}}"></script>
    <script src="{{ URL::asset('public/admin/assets/vendor/libs/swiper/swiper.js')}}"></script>

    <!-- Main JS -->
    <script src="{{ URL::asset('public/admin/assets/js/main.js')}}"></script>

    <!-- Page JS -->
    <script src="{{ URL::asset('public/admin/assets/js/dashboards-analytics.js')}}"></script>
</body>

</html>
