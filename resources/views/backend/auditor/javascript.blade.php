<script>
    $(document).ready(function() {
        cekStatusAuditor()
    });
</script>
<script>
    function cekStatusAuditor(){
        var id_auditor = $("#id_auditor").val();
        $.ajax({
            type: 'GET',
            url: "{{ route('evaluasi-auditor.cek-status-auditor') }}",
            data:({id_auditor:id_auditor}),
            dataType:'json',
                success: function(response) {
                    
                    if(response.type == 'error' && response.status == 0){
                        Swal.fire({
                            type: response.type,
                            title: response.title,
                            text: response.message,
                            showConfirmButton: true
                        }).then(function (result) {
                            window.location.href = "{{route('biodata.show')}}"
                        });
                    }else if(response.type == 'error' && response.status == 1){
                        Swal.fire({
                            type: response.type,
                            title: response.title,
                            text: response.message,
                            showConfirmButton: true
                        }).then(function (result) {
                            window.location.href = "{{route('dashboard')}}"
                        });
                    }else{
                        Swal.fire({
                            type: response.type,
                            title: response.title,
                            text: response.message,
                            showConfirmButton: true
                        });
                    }
                    
                }
            });
    }
</script>

        