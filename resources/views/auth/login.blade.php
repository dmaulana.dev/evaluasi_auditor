<!DOCTYPE html>

<html
lang="en"
class="light-style customizer-hide"
dir="ltr"
data-theme="theme-default"
data-assets-path="{{ URL::asset('public/admin/assets')}}"
data-template="vertical-menu-template-free"
>
<head>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>Login Pages | Evaluasi Auditor</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{ URL::asset('public/admin/assets/img/favicon/favicon.ico')}}" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet" />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/fonts/boxicons.css')}}" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/core.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/theme-default.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/demo.css')}}" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css')}}" />

    <!-- Page CSS -->
    <!-- Page -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/pages/page-auth.css')}}" />
    <!-- Helpers -->
    <script src="{{ URL::asset('public/admin/assets/vendor/js/helpers.js')}}"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
        <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
        <script src="{{ URL::asset('public/admin/assets/js/config.js')}}"></script>
    </head>

    <body>
        <!-- Content -->
        <div class="container-xxl">
            <div class="authentication-wrapper authentication-basic container-p-y">
                <div class="authentication-inner">
                    <!-- Register -->
                    <form id="submit" method="POST">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="mb-1">Selamat datang! 👋</h4>
                                <p class="mb-4">Silahkan masuk dengan akun anda.</p>

                                <div class="mb-5">
                                    <div class="mb-3">
                                        <label for="username" class="form-label">Username</label>
                                        <input
                                        type="text"
                                        class="form-control"
                                        id="username"
                                        name="username"
                                        placeholder="Masukkan username anda"
                                        autofocus
                                        />
                                    </div>
                                    <div class="mb-3 form-password-toggle">
                                        <div class="d-flex justify-content-between">
                                            <label class="form-label" for="password">Password</label>
                                            <a href="auth-forgot-password-basic.html">
                                                <small>Lupa Password?</small>
                                            </a>
                                        </div>
                                        <div class="input-group input-group-merge">
                                            <input
                                            type="password"
                                            id="password"
                                            class="form-control"
                                            name="password"
                                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                            aria-describedby="password"
                                            />
                                            <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" id="remember-me" />
                                            <label class="form-check-label" for="remember-me"> Ingatkan saya </label>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <button class="btn btn-login btn-primary d-grid w-100" type="submit">Masuk</button>
                                    </div>
                                </div>

                                <p class="text-center">
                                    <span>Belum memiliki akun?</span>
                                    <a href="{{route('register')}}">
                                        <span>Buat disini</span>
                                    </a>
                                </p>
                            </div>
                        </div>
                        <!-- /Register -->
                    </div>
                </form>
                <!-- / Content -->
            </div>
        </div>

        <!-- Core JS -->
        <!-- build:js assets/vendor/js/core.js -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>

        <script>
            $("#submit").submit(function(e) {
                e.preventDefault();
                var username = $("#username").val();
                var password = $("#password").val();

                Swal.fire({
                    title: 'Please Wait !',
                    allowOutsideClick: false,
                    showCancelButton: false,
                    showConfirmButton: false,
                    // html: `<div><i class="animate-spin fas fa-spinner"></i></div>`,
                    didOpen: () => {
                        Swal.showLoading()
                    },
                });

                $.ajax({
                    url: "{{ route('api.login') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: {
                        "username": username,
                        "password": password
                    },
                    success: function(response) {
                        if (response.type == "success") {

                            localStorage.token = response.token;

                            console.log(response);

                            Swal.fire({
                                type: 'success',
                                title: 'Login Berhasil!',
                                text: 'Anda akan di arahkan dalam 3 Detik',
                                timer: 3000,
                                showCancelButton: false,
                                showConfirmButton: false
                            }).then (function() {
                                window.location.href = response.redirect;
                            });
                        } else {
                            Swal.fire({
                                type: 'info',
                                title: 'Username atau password salah!',
                                text: "Please check your username or password!",
                                showCancelButton: false,
                                showConfirmButton: false
                            }).then (function() {
                                window.location.href = "{{ route('auth.login') }}";
                            });
                        }
                    },
                    error: function(jqXhr, json, errorThrown) {
                        Swal.fire({
                            type: 'error',
                            title: 'Login Gagal!',
                            text: "Please check your username or password!",
                            showCancelButton: false,
                            showConfirmButton: false
                        }).then (function() {
                            window.location.href = "{{ route('auth.login') }}";
                        });
                    }
                });
            });
        </script>

        <script src="{{ URL::asset('public/admin/assets/vendor/libs/jquery/jquery.js')}}"></script>
        <script src="{{ URL::asset('public/admin/assets/vendor/libs/popper/popper.js')}}"></script>
        <script src="{{ URL::asset('public/admin/assets/vendor/js/bootstrap.js')}}"></script>
        <script src="{{ URL::asset('public/admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>

        <script src="{{ URL::asset('public/admin/assets/vendor/js/menu.js')}}"></script>
        <!-- endbuild -->

        <!-- Vendors JS -->

        <!-- Main JS -->
        <script src="{{ URL::asset('public/admin/assets/js/main.js')}}"></script>

        <!-- Page JS -->

        <!-- Place this tag in your head or just before your close body tag. -->
        <script async defer src="https://buttons.github.io/buttons.js"></script>
    </body>
    </html>