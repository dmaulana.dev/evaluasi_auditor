<!DOCTYPE html>

<html
lang="en"
class="light-style customizer-hide"
dir="ltr"
data-theme="theme-default"
data-assets-path="{{ URL::asset('public/admin/assets')}}"
data-template="vertical-menu-template-free"
>
<head>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>Register Pages | Evaluasi Auditor</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{ URL::asset('public/admin/assets/img/favicon/favicon.ico')}}" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet" />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/fonts/boxicons.css')}}" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/core.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/theme-default.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/demo.css')}}" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css')}}" />

    <!-- Page CSS -->
    <!-- Page -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/pages/page-auth.css')}}" />
    <!-- Helpers -->
    <script src="{{ URL::asset('public/admin/assets/vendor/js/helpers.js')}}"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
        <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
        <script src="{{ URL::asset('public/admin/assets/js/config.js')}}"></script>
    </head>

    <body>
        <!-- Content -->
        <div class="container-xxl">
            <div class="authentication-wrapper authentication-basic container-p-y">
                <div class="authentication-inner">
                    <!-- Register -->
                    <form id="submit" method="POST">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="mb-1">Silahkan isi form dibawah ini</h4>

                            <div class="mb-6">
                                    <div class="mb-3">
                                        <label for="nip" class="form-label">NIP</label>
                                        <input
                                        type="text"
                                        class="form-control"
                                        id="nip"
                                        name="nip"
                                        placeholder="Masukkan NIP anda"
                                        autofocus
                                        onchange="ValidateNumber()"
                                        required
                                        />
                                    </div>
                                                                    
                                    <div class="mb-3">
                                        <label for="fullname" class="form-label">Nama Lengkap</label>
                                        <input
                                        type="text"
                                        class="form-control"
                                        id="fullname"
                                        name="fullname"
                                        placeholder="Masukkan Nama Lengkap anda"
                                        autofocus
                                        required
                                        />
                                    </div>
                                    
                                    <div class="mb-3 form-password-toggle">
                                        <div class="d-flex justify-content-between">
                                            <label class="form-label" for="password">Password</label>
                                        </div>
                                        <div class="input-group input-group-merge">
                                            <input
                                            type="password"
                                            id="password"
                                            class="form-control"
                                            name="password"
                                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                            aria-describedby="password"
                                            required
                                            />
                                            <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                                        </div>
                                    </div>
                                    <div class="mb-3 form-password-toggle">
                                        <div class="d-flex justify-content-between">
                                            <label class="form-label" for="password">Ulangi Password</label>
                                        </div>
                                        <div class="input-group input-group-merge">
                                            <input
                                            type="password"
                                            id="password_ulang"
                                            class="form-control"
                                            name="password_ulang"
                                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                            aria-describedby="password"
                                            required
                                            />
                                            <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                                        </div>
                                    </div>
                                    

                                    <div class="mb-3">
                                        <button class="btn btn-login btn-primary d-grid w-100" type="submit">Daftar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Register -->
                    </div>
                </form>
                <!-- / Content -->
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
        <script src="{{ URL::asset('public/admin/assets/vendor/libs/jquery/jquery.js')}}"></script>
        <script src="{{ URL::asset('public/admin/assets/vendor/libs/popper/popper.js')}}"></script>
        <script src="{{ URL::asset('public/admin/assets/vendor/js/bootstrap.js')}}"></script>
        <script src="{{ URL::asset('public/admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>

        <script src="{{ URL::asset('public/admin/assets/vendor/js/menu.js')}}"></script>
        <script src="{{ URL::asset('public/admin/assets/js/main.js')}}"></script>
        <script async defer src="https://buttons.github.io/buttons.js"></script>
        <script>
            $(document).ready(function(){
                $("#nip").val('');
                $("#fullname").val('');
                $("#password").val('');
                $("#password_ulang").val('');
            });
        </script>
        <script>
            $("#submit").submit(function(e) {
                e.preventDefault();
                var password = $("#password").val();
                var password_ulang = $("#password_ulang").val();

                if(password !== password_ulang){
                    Swal.fire({
                        type: 'info',
                        title: 'password ulang anda tidak sesuai!',
                        text: "silahkan masukkan ulang password anda!",
                        showCancelButton: false,
                        showConfirmButton: true
                    })
                    $("#password").val('');
                    $("#password_ulang").val('');
                }else{
                    onSave()
                    
                }
            });
        </script>
        <script>
            function onSave(){
                var nip = $("#nip").val();
                var fullname = $("#fullname").val();
                var password = $("#password").val();
                var password_ulang = $("#password_ulang").val();
                $.ajax({
                    url: "{{ route('api.registrasi') }}",
                    type: "POST",
                    dataType: "JSON",
                    cache: false,
                    data: {
                        "nip": nip,                      
                        "fullname": fullname,
                        "password": password,
                        "password_ulang":password_ulang
                    },
                    success: function(response) {
                        if(response.type == 'success'){
                            Swal.fire({
                            type: response.type,
                            title: response.title,
                            text: response.message,
                            showConfirmButton: true
                        }).then (function() {
                            window.location = "/login"
                        });
                        }else{
                            Swal.fire({
                            type: response.type,
                            title: response.title,
                            text: response.message,
                            showConfirmButton: true
                            });
                        }
                    }
                });
            }
        </script>
        <script>
            function ValidateNumber(){
                var validasiAngka = /^[0-9]+$/;
                var nip = $("#nip").val();
                if (nip.match(validasiAngka)) {
                    return true
                }else{
                    Swal.fire({
                        type: 'warning',
                        title: 'Format Salah',
                        text: 'NIP hanya boleh angka',
                        showConfirmButton: true
                    });
                    $("#nip").val('')
                    return false
                }
            }
        </script>
    </body>
    </html>