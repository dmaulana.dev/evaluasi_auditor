<!DOCTYPE html>

<html lang="en" class="light-style layout-navbar-fixed layout-menu-fixed" dir="ltr" data-theme="theme-default"
data-assets-path="{{ URL::asset('public/admin/assets')}}" data-template="vertical-menu-template">

<head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Pengaturan Lainnya | Evaluasi Auditor</title>
    
    <meta name="description" content="" />
    
    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{ URL::asset('public/admin/assets/img/favicon/favicon.ico')}}" />
    
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap"
    rel="stylesheet" />
    
    <!-- Icons -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/fonts/materialdesignicons.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/fonts/fontawesome.css')}}" />
    <!-- Menu waves for no-customizer fix -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/node-waves/node-waves.css')}}" />
    
    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/rtl/core.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/css/rtl/theme-default.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/css/demo.css')}}" />
    
    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/typeahead-js/typeahead.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/datatables-bs5/datatables.bootstrap5.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/fonts/boxicons.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/datatables-responsive-bs5/responsive.bootstrap5.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/datatables-checkboxes-jquery/datatables.checkboxes.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/datatables-buttons-bs5/buttons.bootstrap5.css')}}" />
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/flatpickr/flatpickr.css')}}" />
    <!-- Row Group CSS -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/datatables-rowgroup-bs5/rowgroup.bootstrap5.css')}}" />
    <!-- Form Validation -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin/assets/vendor/libs/formvalidation/dist/css/formValidation.min.css')}}" />
    
    <link rel="stylesheet" href="{{ URL::asset('public/custom/custom.css') }}" />
    
    <!-- Page CSS -->
    
    <!-- Helpers -->
    <script src="{{ URL::asset('public/admin/assets/vendor/js/helpers.js')}}"></script>
    {{-- <script src="{{ URL::asset('public/admin/assets/vendor/js/template-customizer.js')}}"></script> --}}
    <script src="{{ URL::asset('public/admin/assets/js/config.js')}}"></script>
    
    <script>
        const showLoading = () => Swal.fire({
            title: 'Please Wait !',
            allowOutsideClick: false,
            showCancelButton: false,
            showConfirmButton: false,
            // html: `<div><i class="animate-spin fas fa-spinner"></i></div>`,
            didOpen: () => {
                Swal.showLoading()
            },
        });
    </script>
</head>

<body>
    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">
            
            <!-- Menu -->
            @include('layouts.admin._menu-admin')
            <!-- / Menu -->
            
            <!-- Layout container -->
            <div class="layout-page">
                @include('layouts.admin._navbar-admin')
                
                <!-- / Navbar -->
                
                <!-- Content wrapper -->
                <div class="content-wrapper">
                    
                    @yield('content')
                    
                    <!-- Footer -->
                    @include('layouts.admin._footer-admin')
                    <!-- / Footer -->
                    
                    <div class="content-backdrop fade"></div>
                </div>
                <!-- Content wrapper -->
            </div>
            <!-- / Layout page -->
        </div>
        
        <!-- Overlay -->
        <div class="layout-overlay layout-menu-toggle"></div>
        
        <!-- Drag Target Area To SlideIn Menu On Small Screens -->
        <div class="drag-target"></div>
    </div>
    <!-- / Layout wrapper -->
    
    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
    
    @include('sweetalert::alert')
    
    <script src="{{URL::asset('public/admin/assets/vendor/libs/jquery/jquery.js')}}"></script>
    <script src="{{URL::asset('public/admin/assets/vendor/libs/popper/popper.js')}}"></script>
    <script src="{{URL::asset('public/admin/assets/vendor/js/bootstrap.js')}}"></script>
    <script src="{{URL::asset('public/admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{URL::asset('public/admin/assets/vendor/libs/node-waves/node-waves.js')}}"></script>
    
    <script src="{{URL::asset('public/admin/assets/vendor/libs/hammer/hammer.js')}}"></script>
    <script src="{{URL::asset('public/admin/assets/vendor/libs/i18n/i18n.js')}}"></script>
    <script src="{{URL::asset('public/admin/assets/vendor/libs/typeahead-js/typeahead.js')}}"></script>
    
    <script src="{{URL::asset('public/admin/assets/vendor/js/menu.js')}}"></script>
    <!-- endbuild -->
    
    <!-- Vendors JS -->
    <script src="{{URL::asset('public/admin/assets/vendor/libs/datatables-bs5/datatables-bootstrap5.js')}}"></script>
    
    <!-- Flat Picker -->
    <script src="{{URL::asset('public/admin/assets/vendor/libs/moment/moment.js')}}"></script>
    <script src="{{URL::asset('public/admin/assets/vendor/libs/flatpickr/flatpickr.js')}}"></script>
    
    <!-- Form Validation -->
    <script src="{{URL::asset('public/admin/assets/vendor/libs/formvalidation/dist/js/FormValidation.min.js')}}">
    </script>
    <script src="{{URL::asset('public/admin/assets/vendor/libs/formvalidation/dist/js/plugins/Bootstrap5.min.js')}}">
    </script>
    <script src="{{URL::asset('public/admin/assets/vendor/libs/formvalidation/dist/js/plugins/AutoFocus.min.js')}}">
    </script>
    
    <!-- Main JS -->
    <script src="{{URL::asset('public/admin/assets/js/main.js')}}"></script>
    
    <!-- Page JS -->
    {{-- <script src="{{URL::asset('public/admin/assets/js/modal-add-role.js')}}"></script> --}}
    
    @stack('custom-scripts')
    
    <script>
        const showLoading = () => Swal.fire({
            title: 'Please Wait !',
            allowOutsideClick: false,
            showCancelButton: false,
            showConfirmButton: false,
            html: '',
            type: 'info',
            onOpen: () => {
                Swal.showLoading()
            },
        });
    </script>
    
    <script>
        function bin2hex(s) {
            var v, i, f = 0,
            a = [];
            s += '';
            f = s.length;
            
            for (i = 0; i < f; i++) {
                a[i] = s.charCodeAt(i).toString(16).replace(/^([\da-f])$/, "0$1");
            }
            
            return a.join('');
        }
        
        Date.prototype.addDays = function(days) {
            var date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        }
    </script>
    
</body>

</html>
