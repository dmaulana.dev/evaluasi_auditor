<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\ConfigurationController;
use App\Http\Controllers\Admin\DashboardAdminController;
use App\Http\Controllers\Admin\RefPelatihanController;
use App\Http\Controllers\User\DashboardUserController;
use App\Http\Controllers\Admin\MasterAdminController;
use App\Http\Controllers\Admin\MasterUserController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Auditor\PelatihanAuditorController;
use App\Http\Controllers\Master\MasterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [AuthController::class, 'index'])->name('auth.login');
Route::get('/login', [AuthController::class, 'index'])->name('auth.login');
Route::get('/register', [AuthController::class, 'register'])->name('register');

Route::middleware(['has_login'])->group(function () {
    // Dashboard Admin
    Route::get('/dashboard', [DashboardAdminController::class, 'index'])->name('dashboard');
    Route::get('/dashboard-admin', [DashboardAdminController::class, 'index'])->name('dashboard.admin');
    
    // master Data
    Route::get('/master-admin', [MasterAdminController::class, 'index'])->name('master-admin.index');
    Route::get('/master-admin/create', [MasterAdminController::class, 'create'])->name('master-admin.create');
    Route::post('/master-admin/store', [MasterAdminController::class, 'store'])->name('master-admin.store');
    Route::get('/master-admin/edit/{id}', [MasterAdminController::class, 'edit'])->name('master-admin.edit');
    Route::post('/master-admin/update/{id}', [MasterAdminController::class, 'update'])->name('master-admin.update');
    Route::get('/master-admin/show/{id}', [MasterAdminController::class, 'show'])->name('master-admin.show');
    Route::get('/master-admin/destroy/{id}', [MasterAdminController::class, 'destroy'])->name('master-admin.destroy');
    
    //user or pengguna
    Route::get('/dashboard-user', [DashboardUserController::class, 'index'])->name('dashboard.user');
    
    // master user
    Route::get('/master-user', [MasterUserController::class, 'index'])->name('master-user.index');
    Route::get('/master-user/create', [MasterUserController::class, 'create'])->name('master-user.create');
    Route::post('/master-user/store', [MasterUserController::class, 'store'])->name('master-user.store');
    Route::get('/master-user/edit/{id}', [MasterUserController::class, 'edit'])->name('master-user.edit');
    Route::post('/master-user/update/{id}', [MasterUserController::class, 'update'])->name('master-user.update');
    Route::get('/master-user/show/{id}', [MasterUserController::class, 'show'])->name('master-user.show');
    Route::get('/master-user/destroy/{id}', [MasterUserController::class, 'destroy'])->name('master-user.destroy');
    
    // config
    Route::get('/configuration', [ConfigurationController::class, 'index'])->name('configuration.index');
    
    // Permission
    Route::get('/permission', [PermissionController::class, 'index'])->name('permission.index');
    Route::get('/permission/create', [PermissionController::class, 'create'])->name('permission.create');
    Route::post('/permission/store', [PermissionController::class, 'store'])->name('permission.store');
    Route::get('/permission/show/{id}', [PermissionController::class, 'show'])->name('permission.show');
    Route::get('/permission/edit/{id}', [PermissionController::class, 'edit'])->name('permission.edit');
    Route::post('/permission/update/{id}', [PermissionController::class, 'update'])->name('permission.update');
    Route::get('/permission/destroy/{id}', [PermissionController::class, 'destroy'])->name('permission.destroy');
    
    // role
    Route::get('/role', [RoleController::class, 'index'])->name('role.index');
    Route::post('/role/store', [RoleController::class, 'store'])->name('role.store');
    Route::get('/role/show/{id}', [RoleController::class, 'show'])->name('role.show');
    Route::get('/role/show-edit/{id}', [RoleController::class, 'showedit'])->name('role.show-edit');
    Route::post('/role/update/{id}', [RoleController::class, 'update'])->name('role.update');
    Route::get('/role/destroy/{id}', [RoleController::class, 'destroy'])->name('role.destroy');
    
    
    // master ref pelatihan
    Route::get('/ref-pelatihan', [RefPelatihanController::class, 'index'])->name('ref-pelatihan.index');
    Route::get('/ref-pelatihan/create', [RefPelatihanController::class, 'create'])->name('ref-pelatihan.create');
    Route::post('/ref-pelatihan/store', [RefPelatihanController::class, 'store'])->name('ref-pelatihan.store');
    Route::get('/ref-pelatihan/edit/{id}', [RefPelatihanController::class, 'edit'])->name('ref-pelatihan.edit');
    Route::post('/ref-pelatihan/update/{id}', [RefPelatihanController::class, 'update'])->name('ref-pelatihan.update');
    Route::get('/ref-pelatihan/show/{id}', [RefPelatihanController::class, 'show'])->name('ref-pelatihan.show');
    Route::get('/ref-pelatihan/destroy/{id}', [RefPelatihanController::class, 'destroy'])->name('ref-pelatihan.destroy');
    
    // master ref pertanyaan
    Route::get('/ref-pertanyaan', [RefPelatihanController::class, 'index'])->name('ref-pertanyaan.index');
    Route::get('/ref-pertanyaan/create', [RefPelatihanController::class, 'create'])->name('ref-pertanyaan.create');
    Route::post('/ref-pertanyaan/store', [RefPelatihanController::class, 'store'])->name('ref-pertanyaan.store');
    Route::get('/ref-pertanyaan/edit/{id}', [RefPelatihanController::class, 'edit'])->name('ref-pertanyaan.edit');
    Route::post('/ref-pertanyaan/update/{id}', [RefPelatihanController::class, 'update'])->name('ref-pertanyaan.update');
    Route::get('/ref-pertanyaan/show/{id}', [RefPelatihanController::class, 'show'])->name('ref-pertanyaan.show');
    Route::get('/ref-pertanyaan/destroy/{id}', [RefPelatihanController::class, 'destroy'])->name('ref-pertanyaan.destroy');
    
    // master grade / penilaian
    Route::get('/master-penilaian', [RefPelatihanController::class, 'index'])->name('master-penilaian.index');
    Route::get('/master-penilaian/create', [RefPelatihanController::class, 'create'])->name('master-penilaian.create');
    Route::post('/master-penilaian/store', [RefPelatihanController::class, 'store'])->name('master-penilaian.store');
    Route::get('/master-penilaian/edit/{id}', [RefPelatihanController::class, 'edit'])->name('master-penilaian.edit');
    Route::post('/master-penilaian/update/{id}', [RefPelatihanController::class, 'update'])->name('master-penilaian.update');
    Route::get('/master-penilaian/show/{id}', [RefPelatihanController::class, 'show'])->name('master-penilaian.show');
    Route::get('/master-penilaian/destroy/{id}', [RefPelatihanController::class, 'destroy'])->name('master-penilaian.destroy');
    
    // Master profile biodata
    // TAMBAH RESTU
    Route::get('/master-biodata/biodata', [MasterUserController::class, 'showBiodata'])->name('biodata.show');
    Route::post('/master-biodata/store', [MasterUserController::class, 'storeBiodata'])->name('biodata.store');
    
    // EVALUASI PESERTA
    // TAMBAH RESTU
    Route::get('/evaluasi-auditor/pelatihanA', [PelatihanAuditorController::class, 'showPelatihanA'])->name('evaluasi-auditor.pelatihanA');
    Route::get('/evaluasi-auditor/cek-status-auditor', [PelatihanAuditorController::class, 'cekStatusAuditor'])->name('evaluasi-auditor.cek-status-auditor');
    // Logout
    Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');
});

// TAMBAH RESTU
Route::get('/master/provinsi', [MasterController::class, 'provinsi'])->name('master.provinsi');
Route::get('/master/kabupaten', [MasterController::class, 'kabupaten'])->name('master.kabupaten');
Route::get('/master/kecamatan', [MasterController::class, 'kecamatan'])->name('master.kecamatan');
Route::get('/master/kelurahan', [MasterController::class, 'kelurahan'])->name('master.kelurahan');

Route::get('/404', function(){
    abort(404);
})->name('not_found');


