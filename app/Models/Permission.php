<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'permission_id',
        'visible'
    ];

    protected $table = 'permissions';

    public function children()
    {
        return $this->hasMany('App\Models\Permission', 'parent_id', 'id')->orderBy('order_number');
    }

    public function details(){
        return $this->hasMany('App\Models\PermissionDetails', 'permission_id', 'id');
    }
}
