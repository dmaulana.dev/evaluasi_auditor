<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    
    public $dates = ['deleted_at'];

    protected $hidden = [
        'password',
        'api_token'
    ];

    protected $table = 'users';

    // protected $appends = ['photo_url'];

    // public function getPhotoUrlAttribute()
    // {
    //     return Storage::drive('images')->exists($this->photo)
    //     ? url('storage/images/'.$this->photo) : null;
    // }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
}
