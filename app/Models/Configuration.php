<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $dates = [
        'deleted_at'
    ];

    protected $casts = [
        'value' => 'array',
    ];

    protected $table = 'configurations';

    public function scopeGetValue($query, $config)
    {
        $data = $query->where('variable', $config)->first();
        return $data->value;

    }
}
