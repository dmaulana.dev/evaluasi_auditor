<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfileUser extends Model
{
    protected $table = 'profile_user';

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
