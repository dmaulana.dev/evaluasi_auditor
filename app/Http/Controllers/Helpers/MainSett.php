<?php

namespace App\Http\Controllers\Helpers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Crypt;
use Firebase\JWT\JWT;
use Session;
// use LengthAwarePaginator;
// use Paginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class MainSett extends Controller
{
   
    public static function GenerateJWT($data)
    {
        $key = env('JWT_SECRET');
        $data['timestamp'] = Carbon::now('Asia/Jakarta')->timestamp;
        $payload = array(
            "iss" => "Sikemet-Backend",
            "aud" => "Sikemet-BackendApi",
            "iat" => Carbon::now('Asia/Jakarta')->timestamp,
            "nbf" => Carbon::now('Asia/Jakarta')->timestamp,
            "exp" => Carbon::now('Asia/Jakarta')->addYears(1)->timestamp,
            "data" => $data
        );
        return JWT::encode($payload, $key, 'HS256');
    }

//    public static function RefreshToken($data){
//        $key = env('JWT_SECRET_REFRESH');
//        $data['timestamp'] = Carbon::now('Asia/Jakarta')->timestamp;
//        $payload = array(
//            "iss" => "Sikemet-Backend",
//            "aud" => "Sikemet-BackendApi",
//            "iat" => Carbon::now('Asia/Jakarta')->timestamp,
//            "nbf" => Carbon::now('Asia/Jakarta')->timestamp,
//            "exp" => Carbon::now('Asia/Jakarta')->addYears(10)->timestamp,
//            "data" => $data
//        );
//        return JWT::encode( $payload, $key, 'HS256' );
//    }

    public static function DecodeJWT($token)
    {
        return JWT::decode($token, env('JWT_SECRET'), ['HS256']);
    }

//    public static function DecodeJWTRefresh($token){
//        $decoded = JWT::decode($token, env('JWT_SECRET_REFRESH'), ['HS256']);
//        return $decoded;
//    }

    // public static function censorString($target) {
    //     $count = strlen($target) - 7;
    //     $output = substr_replace($target, str_repeat('*', $count), 4, $count);
    //     return $output;
    // }

    public static function censorString($string){
        $length = strlen($string) - floor(strlen($string) / 2);
        $start = floor($length / 2);
        $replacement = str_repeat('*', $length);
        return substr_replace($string, $replacement, $start, $length);
    }


    public static function getUserDataSession()
    {
        return Session::get('data_user');
    }

    public static function md5($string)
    {
        return md5($string);
    }

    public static function rupiah($angka)
    {
        $angka = intval($angka);

        $hasil_rupiah = "Rp " . number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }

    public static function formatNumber($angka)
    {
        $angka = intval($angka);

        $hasil_rupiah = number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }

    static function buildNumber()
    {
        try {
            $fh = fopen('../buildnumber.txt', 'r');
            $buildNumber = 0;
            while ($line = fgets($fh)) {
                $buildNumber = $line;
                break;
            }
            fclose($fh);
            return $buildNumber;
        } catch (\Throwable $th) {
            return "Development Mode";
        }
    }

    static function decodeToken($token)
    {
        try {
            $decode = Crypt::decrypt($token);
            $decode['id'] = self::decodeId($decode['id']);
            $decode['validate_until'] = self::decodeId($decode['validate_until']);
            $decode['data'] = json_decode($decode['data']);
            return $decode;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public static function decodeHeader($header) {
        $auth = explode(" ", $header);
        $token = end($auth);
        $decode = self::decodeToken($token);
        $dateExpire = Carbon::parse($decode['validate_until']);

        return [
            "decode" => $decode,
            "dateExpire" => $dateExpire,
        ];
    }

    public static function decodeId($value)
    {
        try {
            return hex2bin(substr($value, 20));
        } catch (\Throwable $th) {
            abort(404);
        }
    }

    static function encryptData($data, $expire = 1){
        return Crypt::encrypt([
            "data" => json_encode($data),
            "expire" => self::encodeId(Carbon::now('Asia/Jakarta')->addHour($expire)),
        ]);
    }

    static function decryptData($data){
        try {
            $decode = Crypt::decrypt($data);
            $decode['expire'] = self::decodeId($decode['expire']);
            $decode['data'] = json_decode($decode['data']);
            return $decode;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    static function encodeToken($id, $data = [], $refresh = true)
    {
        return \Crypt::encrypt([
            'id' => self::encodeId($id),
            'data' => json_encode($data),
            'refresh' => $refresh,
            'timestamp' => Carbon::now('Asia/Jakarta'),
            'validate_until' => self::encodeId(Carbon::now('Asia/Jakarta')->addHour(2))
        ]);
    }

    public static function encodeId($value)
    {
        return Self::generateRandomString(20) . bin2hex($value);
    }

    public static function generateRandomString($length = 10, $type = 'normal')
    {
        if ($type === 'normal') {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        } else {
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    static function vlidatePhoneNumber($number)
    {
        if (substr($number, 0, 1) === "0") {
            return "+62" . substr($number, 1);
        }

        if (substr($number, 0, 1) === "8") {
            return "+62" . $number;
        }

        if (substr($number, 0, 1) === "+") {
            return $number;
        }

        return $number;
    }

    static function buildMenuTree($flatMenu, $parentId = 0)
    {
        $tree = array();

        foreach ($flatMenu as $item) {
            if ($item['permission_parent_id'] == $parentId) {
                $children = self::buildMenuTree($flatMenu, $item['id']);
                if (!empty($children)) {
                    $item['children'] = $children;
                }
                $tree[] = $item;
            }
        }

        return $tree;
    }

    static function generateMenuList($menuTree)
    {
        if (empty($menuTree)) {
            return '';
        }

        $menuList = '<ul>';

        foreach ($menuTree as $menuItem) {
            $menuList .= '<li>' . $menuItem['title'];

            if (isset($menuItem['children'])) {
                $menuList .= self::generateMenuList($menuItem['children']);
            }

            $menuList .= '</li>';
        }

        $menuList .= '</ul>';

        return $menuList;
    }

    static function menuType($value)
    {
        $data = [
            'menu' => 'Menu',
            'link' => 'Link',
            'sub_link' => 'Sub Link',
            'function' => 'Function',
        ];
        try {
            return "<span class='badge rounded-pill text-bg-primary'>{$data[$value]}</span>";
        } catch (\Throwable $th) {
            return '';
        }
    }

    public static function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

}
