<?php

namespace App\Http\Controllers\Auditor;

use App\Http\Controllers\Controller;
use App\Models\ProfileUser;
use Illuminate\Http\Request;

class PelatihanAuditorController extends Controller
{
    public function showPelatihanA(){     
        return view('backend.auditor.index');
    }

    public function cekStatusAuditor(Request $request){
        $profilAuditor = ProfileUser::where('user_id', $request->id_auditor)->first();
        
        if($profilAuditor->verifikasi == 0){
            return response()->json([
                'type' => 'error',
                'title' => 'Gagal Akses',
                'message' => 'Anda belum mengisi biodata di profil',
                'status' => 0
            ], 200);
        }else if($profilAuditor->verifikasi == 1){
            return response()->json([
                'type' => 'error',
                'title' => 'Profil anda belum terverifikasi',
                'message' => 'Silahkan ditunggu untuk proses verifikasi biodata',
                'status' => 1
            ], 200);
        }else{
            return response()->json([
                'type' => 'success',
                'title' => 'Selamat Datang',
                'message' => 'Silahkan isi form evaluasi dengan benar',
                'status' => 2
            ], 200);
        }
    }
}
