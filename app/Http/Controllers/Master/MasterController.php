<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\MasterKabupaten;
use App\Models\MasterKecamatan;
use App\Models\MasterKelurahan;
use App\Models\MasterProvinsi;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    public function provinsi(){
        $provinsi = MasterProvinsi::select([
            'id',
            'nama',
        ])->where('aktif', 1)->get();
        return response()->json($provinsi);
    }

    public function kabupaten(Request $request){
        $kabupaten = MasterKabupaten::select([
            'id',
            'nama',
        ])->where('id_provinsi', $request->id)->where('aktif', 1)->get();
        return response()->json($kabupaten);
    }

    public function kecamatan(Request $request){
        $kecamatan = MasterKecamatan::select([
            'id',
            'nama',
        ])->where('id_kabupaten', $request->id)->where('aktif', 1)->get();
        return response()->json($kecamatan);
    }

    public function kelurahan(Request $request){
        $kelurahan = MasterKelurahan::select([
            'id',
            'nama',
        ])->where('id_kecamatan', $request->id)->where('aktif', 1)->get();
        return response()->json($kelurahan);
    }
}
