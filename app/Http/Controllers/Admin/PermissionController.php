<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helpers\MainSett;
use App\Models\Permission;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PermissionController extends Controller
{
    
    public function index(Request $request) {
        $data = Permission::orderBy('id', 'desc')->get();
        
        if($request->ajax()){
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<button class="btn btn-sm btn-icon btn-text-primary rounded-pill btn-icon me-2 btn-edit" data-id="'.$row->id.'"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>';
                $btn = $btn.'<button data-id="'.$row->id.'" class="btn btn-sm btn-icon btn-text-danger rounded-pill btn-icon btn-delete"><i class="mdi mdi-delete-outline mdi-20px"></i></button>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        
        
        return view('settings.permission.index');
    }
    
    public function create(Request $request){
        $listPermission = Permission::where('parent_id', null)->get();
        
        return view('settings.permission.create', [
            'list_permission' => $listPermission,
        ]);
        
    }
    
    
    
    public function store(Request $request)
    {
        
        $parent = Permission::find($request->parent_id);
        $permission = new Permission;
        $permission->name = $request->name;
        $permission->description = $request->description;
        $permission->url_name = $request->url_name;
        $permission->url_path = $request->url_path;
        $permission->icon = $request->icon;
        $permission->parent_id = $request->parent_id;
        $permission->parent_name = $parent->name ?? null;
        $permission->order_number = $request->order_number;
        $permission->created_by = auth()->user()->id;
        $permission->save();
        
        // return response()->json([
        //     'type' => 'success',
        //     'message' => 'Permission Berhasil Ditambahkan!',
        // ], 200);
        
        return redirect()->route('permission.index');
    }
    
    public function edit(Request $request, $id)
    {
        $id = MainSett::decodeId($id);
        $permission = Permission::findOrFail($id);
        
        $listPermission = Permission::where('parent_id', null)->get();
        
        return view('settings.permission.edit', [
            'data' => $permission,
            'list_permission' => $listPermission,
        ]);
        // return response()->json([
        //     'type' => 'success',
        //     'data' => $permission
        // ], 200);
    }
    
    public function show(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);
        
        return response()->json([
            'type' => 'success',
            'data' => $permission
        ], 200);
    }
    
    public function update(Request $request, $id)
    {
        $ids = MainSett::decodeId($id);
        $permission = Permission::findOrFail($ids);
        $permission->name = $request->name;
        $permission->description = $request->desc;
        $permission->url_path = $request->url_path;
        $permission->url_name = $request->url_name;
        $permission->icon = $request->icon;
        if ($request->parent_id != null) {
            $parent = Permission::where('id', $request->parent_id)->first();
            $permission->parent_id = $request->parent_id;
            $permission->parent_name = $parent->name;
        }
        $permission->order_number = $request->order_number;
        $permission->updated_by = auth()->user()->updated_by;
        $permission->save();
        
        return redirect()->route('permission.index');
    }
    
    public function destroy(Request $request, $id)
    {
        $ids = MainSett::decodeId($id);
        
        $permission = Permission::where('id', $ids)->first();
        $permission->delete();
        
        return response()->json([
            'type' => 'success',
            'message' => 'Deleted success'
        ]);
        
    }
    
    
    public function listParentId(Request $request)
    {
        $permissions = Permission::whereNull('parent_id')->get();
        
        return response()->json([
            'type' => 'success',
            'data' => $permissions
        ], 200);
    }
    
    public function get()
    {
        $permissions = Permission::all();
        
        return response()->json([
            'type' => 'success',
            'data' => $permissions
        ], 200);
    }
}
