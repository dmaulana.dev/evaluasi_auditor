<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helpers\MainSett;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionDetails;

use DB;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{
    
    public function index(Request $request)
    {
        $listRole = Role::orderBy('id', 'ASC')->get();
        $listPermission = Permission::orderBy('order_number', 'ASC')->get();
        
        if ($request->ajax()) {
            $data = Role::get();
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                if (auth()->user()->role_name == 'superadmin') {
                    $btn = '<button class="btn btn-sm btn-icon btn-text-primary rounded-pill btn-icon me-2 btn-edit" data-id="'.$row->id.'"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>';
                    $btn = $btn.'<button class="btn btn-sm btn-icon btn-text-danger rounded-pill btn-icon me-2 btn-delete" data-id="'.$row->id.'"><i class="mdi mdi-delete-outline mdi-20px"></i></button>';
                }else{
                    $btn = '<button class="btn btn-sm btn-icon btn-text-primary rounded-pill btn-icon me-2 btn-edit" data-id="'.$row->id.'"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>';
                }
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        
        return view('settings.role.index', [
            'permission' => $listPermission,
            'list_role' => $listRole
        ]);
    }
    
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $role = new Role();
            $role->name = $request->roleName;
            $role->description = $request->roleDesc;
            $role->created_by = auth()->user()->full_name;
            $role->save();
            
            foreach (json_decode($request['permission']) as $permission) {
                $cekPermission = Permission::where('id', $permission->id)->first();
                $permissionDetails = new PermissionDetails;
                $permissionDetails->role_id = $role->id;
                $permissionDetails->role_name = $role->name;
                $permissionDetails->permission_id = $permission->id;
                if ($cekPermission->parent_id == null) {
                    $visible = 0;
                } else {
                    $visible = 1;
                }
                $permissionDetails->visible = $visible;
                $permissionDetails->save();
            }
            
            DB::commit();
            
            return response()->json([
                'type' => 'success',
                'message' => 'Role Berhasil Ditambahkan!',
                'request' => $request->all(),
            ], 200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json([
                'type' => 'Error',
                'message' => 'Role Gagal Ditambahkan!',
                'error' => $th->getMessage(),
                'request' => $request->all(),
            ], 500);
        }
    }
    
    public function showedit($id){
        $id = MainSett::decodeId($id);
        
        $data = Role::find($id);
        
        $list_permission_details = PermissionDetails::where('role_id', $data->id)
        ->orderBy('role_id', 'asc')
        ->get();
        
        return view('settings.role.editModal', [
            'data' => $data,
            'list_id' => collect($list_permission_details)->map(function($item){
                return $item->permission_id;
            }),
            'permission' => Permission::orderBy('id', 'ASC')->get(),
        ]);
    }
    
    public function show(Request $request, $id)
    {
        $id = MainSett::decodeId($id);
        $role = Role::findOrFail($id);
        
        $permissions = [];
        foreach ($role->permissions as $permission) {
            $permissions[$permission['permission_id']] = $permission['visible'];
        }
        
        return response()->json([
            'data' => $permissions,
            'type' => 'success',
        ], 200);
    }
    
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        
        try {
            $role = Role::findOrFail($id);
            
            PermissionDetails::where('role_id', $role->id)->each(function($item, $key){
                $item->delete();
            });
            
            $role->name = $request->editRoleName;
            $role->description = $request->editRoleDesc;
            $role->updated_by = auth()->user()->id;
            $role->save();
            
            
            foreach ($request->permission as $index => $permission) {
                $cekPermission = Permission::where('id', $permission)->first();
                $permissionDetails = new PermissionDetails;
                $permissionDetails->role_id = $role->id;
                $permissionDetails->role_name = $role->name;
                $permissionDetails->permission_id = $cekPermission->id;
                if ($cekPermission->parent_id == null) {
                    $visible = 0;
                } else {
                    $visible = 1;
                }
                $permissionDetails->visible = $visible;
                $permissionDetails->save();
            }
            
            DB::commit();
            
            Alert::success("Sukses", "Sukses mengubah Role");
            return redirect()->route('role.index');
        } catch (\Throwable $th) {
            
            Alert::error("Gagal", "Gagal mengubah Role");
            return redirect()->route('role.index');
        }
        
    }
    
    public function destroy($id){
        
        $id = MainSett::decodeId($id);
        
        $data = Role::find($id);
        
        PermissionDetails::where('role_id', $data->id)->each(function($item, $key){
            $item->delete();
        });
        
        $data->delete();
        
        return response()->json([
            'type' => 'success',
            'messgae' => 'Role Berhasil ditambahkan'
        ]);
        
    }
    
    public function list(Request $request)
    {
        $roles = Role::when($request->keyword, function ($query) use ($request) {
            if (!empty($request->keyword)) {
                $query->where('name', 'like', '%' . $request->keyword . '%');
            }
        })
        ->take(10)
        ->get();
        
        return response()->json(
            [
                'type' => 'success',
                'data' => $roles,
            ],
            200,
        );
    }
}
