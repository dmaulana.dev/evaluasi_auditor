<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Configuration;

class ConfigurationController extends Controller
{
    public function index() {
        return view('settings.config.index');
    }

    public function store(Request $request)
    {
        // $request->validate([
        //     'variable' => 'required',
        //     'value' => 'required'
        // ]);

        // $setting = new Settings;

        // $setting->variable = $request->variable;
        // $setting->value = $request->value;
        // $setting->created_by = $request->created_by;
        // $setting->changed_by = $request->changed_by;
        // $setting->save();

        // return response()->json([
        //     'type' => 'success',
        //     'message' => 'Data saved successfully!'
        // ], 201);
    }

    public function show(Request $request, $id)
    {
        $setting = Configuration::findOrFail($id);

        return view('');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'variable' => 'required',
            'value' => 'required'
        ]);

        $setting = Configuration::findOrFail($id);

        $setting->variable = $request->variable;
        $setting->value = $request->value;
        $setting->changed_by = $request->changed_by;
        $setting->save();

        return redirect()->route('');
    }

    public function destroy(Request $request, $id)
    {
        $setting = Configuration::where('_id', $id)->delete();

        return redirect()->route('');
    }

    public function find(Request $request)
    {
        $setting = Configuration::where('variable', $request->variable)->first();

        return response()->json([
            'type' => 'success',
            'data' => $setting->value
        ], 200);
    }
}
