<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helpers\MainSett;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\ProfileUser;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class MasterUserController extends Controller
{
    public function index(Request $request){
        $data = ProfileUser::with('user')->get();
        
        if($request->ajax()){
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2 btn-edit" data-id="'.$row->id.'"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>';
                $btn = $btn.'<button data-id="'.$row->id.'" class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon btn-delete"><i class="mdi mdi-delete-outline mdi-20px"></i></button>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        
        return view('backend.master.user.index');
    }
    
    public function create(Request $request){
        $listRole = Role::get();
        
        return view('backend.master.user.create', [
            'list_role' => $listRole
        ]);
    }
    
    
    public function store(Request $request){
        $role = Role::where('id', $request->role)->first();
        $admin = new User;
        $admin->full_name = $request->nama_lengkap;
        $admin->username = $request->nomor_identitas;
        $admin->password = Hash::make('password00');
        $admin->role_id = $role->id;
        $admin->role_name = $role->name;
        $admin->active = 1;
        $admin->save();
        
        $profilUser = new ProfileUser;
        $profilUser->user_id = $admin->id;
        $profilUser->nama_lengkap = $request->nama_lengkap;
        $profilUser->nomor_identitas = $request->nomor_identitas;
        $profilUser->email = $request->email;
        $profilUser->no_hp = $request->no_hp;
        $profilUser->active = 1;
        $profilUser->save();
        
        return redirect()->route('master-user.index');
    }
    
    
    public function edit(Request $request, $id)
    {
        $listRole = Role::get();
        
        $id = MainSett::decodeId($id);
        $admin = ProfileUser::with('user')->findOrFail($id);
        
        return view('backend.master.user.edit', [
            'list_role' => $listRole,
            'data' => $admin
        ]);
    }
    
    public function update(Request $request, $id)
    {
        $id = MainSett::decodeId($id);
        
        $role = Role::where('id', $request->role)->first();
        
        $profilUser = ProfileUser::where('id', $id)->first();
        
        $user = User::where('id', $profilUser->user_id)->first();
        $user->full_name = $request->nama_lengkap;
        $user->username = $request->nomor_identitas;
        $user->password = Hash::make('password00');
        $user->role_id = $role->id;
        $user->role_name = $role->name;
        $user->active = 1;
        $user->save();
        
        $profilUser->nama_lengkap = $request->nama_lengkap;
        $profilUser->nomor_identitas = $request->nomor_identitas;
        $profilUser->email = $request->email;
        $profilUser->no_hp = $request->no_hp;
        $profilUser->active = 1;
        $profilUser->save();
        
        return redirect()->route('master-user.index');
    }
    
    
    public function details(Request $request, $id)
    {
        $admin = ProfileUser::findOrFail($id);
        return view('backend.master.user.show', [
            'admin' => $admin
        ]);
    }
    
    
    public function destroy(Request $request, $id)
    {
        $id = MainSett::decodeId($id);
        $admin = ProfileUser::where('id', $id)->first();
        
        $user = User::where('id', $admin->user_id)->first();
        $user->delete();
        
        $admin->delete();
        
        return response()->json([
            'type' => 'success',
            'message' => 'delete sukses'
        ], 201);
    }

    public function showBiodata(){
        $profil_id = auth()->user()->id;
        $listProfil = ProfileUser::where('user_id', $profil_id)->first();
        
        return view('backend.master.user.show-biodata', [
            'list_profil' => $listProfil
        ]);

    }

    public function storeBiodata(Request $request){

        $fileAvatar = $request->file('avatar');
        $fileIjazah = $request->file('ijazah');
        
        $filenameAvatar = rand(0, 99).time(). '_' . $fileAvatar->getClientOriginalName();
        $fileAvatarEkstensi = $fileAvatar->getClientOriginalExtension();
        $fileAvatarSize = $fileAvatar->getSize();

        if($fileAvatarEkstensi != 'png' && $fileAvatarEkstensi != 'jpg' && $fileAvatarEkstensi != 'jpeg'){
            return response()->json([
                'type' => 'error',
                'title' => 'Warning',
                'message' => 'File Ekstensi Foto Avatar Tidak Sesuai',
            ], 200);
        }
        // 500000 = 500Kb
        if($fileAvatarSize > 500000){
            return response()->json([
                'type' => 'error',
                'title' => 'Warning',
                'message' => 'File Ukuran Foto Avatar Terlalu Besar',
            ], 200);
        }        
        
        $filenameIjazah = rand(0, 99).time(). '_' . $fileIjazah->getClientOriginalName();
        $fileIjazahEkstensi = $fileIjazah->getClientOriginalExtension();
        $fileIjazahSize = $fileIjazah->getSize();
        $file_path = "public/admin/ijazah/".$filenameAvatar;
        $file_serverpath = asset($file_path);

        if($fileIjazahEkstensi != 'png' && $fileIjazahEkstensi != 'jpg' && $fileIjazahEkstensi != 'jpeg'){
            return response()->json([
                'type' => 'error',
                'title' => 'Warning',
                'message' => 'File Ekstensi Foto Ijazah Tidak Sesuai',
            ], 200);
        }
        // 1000000 = 1MB
        if($fileIjazahSize > 1000000){
            return response()->json([
                'type' => 'error',
                'title' => 'Warning',
                'message' => 'File Ukuran Foto Ijazah Terlalu Besar',
            ], 200);
        }        

        // UPLOAD EKSEKUSI
        $fileAvatar->move('public/admin/assets/img/avatars', $filenameAvatar);
        $fileIjazah->move('public/admin/ijazah', $filenameIjazah);

        $profil = ProfileUser::where('id', $request->id_profil)->first();
        $profil->nama_lengkap = $request->nama_lengkap;
        $profil->email = $request->email;
        $profil->no_hp = $request->ponsel;
        $profil->provinsi = $request->provinsi;
        $profil->kabupaten = $request->kabupaten;
        $profil->kecamatan = $request->kecamatan;
        $profil->kelurahan = $request->kelurahan;
        $profil->rt = $request->nort;
        $profil->rw = $request->norw;
        $profil->alamat = $request->alamat;
        $profil->pendidikan = $request->pendidikan;
        $profil->photo = $filenameAvatar;
        $profil->file_ijazah = $filenameIjazah;
        $profil->url_ijazah = $file_serverpath;
        $profil->verifikasi = 1;
        $profil->update();

        return response()->json([
            'type' => 'success',
            'title' => 'Berhasil',
            'message' => 'Silahkan menunggu verifikasi dari Admin!',
        ], 200);

    }
}
