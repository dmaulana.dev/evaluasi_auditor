<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Permission;

class ProfileController extends Controller
{
    public function index() {
        return view('backend.settings.profile.index');
    }
}
