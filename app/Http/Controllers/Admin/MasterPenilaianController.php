<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helpers\MainSett;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\ProfileAdmin;
use App\Models\Role;
use App\Models\User;
use Yajra\DataTables\DataTables;

class MasterPenilaianController extends Controller
{
    public function index(Request $request){
        $data = ProfileAdmin::with('user')->get();
        
        if($request->ajax()){
            return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $btn = '<button class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon me-2 btn-edit" data-id="'.$row->id.'"><i class="mdi mdi-pencil-outline mdi-20px"></i></button>';
                $btn = $btn.'<button data-id="'.$row->id.'" class="btn btn-sm btn-icon btn-text-secondary rounded-pill btn-icon btn-delete"><i class="mdi mdi-delete-outline mdi-20px"></i></button>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        
        return view('backend.master.admin.index');
        
    }
    
    public function create(Request $request){
        $listRole = Role::get();
        
        return view('backend.master.admin.create', [
            'list_role' => $listRole
        ]);
    }
    
    
    public function store(Request $request){
        $role = Role::where('id', $request->role)->first();
        $admin = new User;
        $admin->full_name = $request->nama_lengkap;
        $admin->username = $request->nomor_identitas;
        $admin->password = Hash::make('password00');
        $admin->role_id = $role->id;
        $admin->role_name = $role->name;
        $admin->active = 1;
        $admin->save();
        
        $profilAdmin = new ProfileAdmin;
        $profilAdmin->user_id = $admin->id;
        $profilAdmin->nama_lengkap = $request->nama_lengkap;
        $profilAdmin->nomor_identitas = $request->nomor_identitas;
        $profilAdmin->email = $request->email;
        $profilAdmin->no_hp = $request->no_hp;
        $profilAdmin->active = 1;
        $profilAdmin->save();
        
        return redirect()->route('master-admin.index');
    }
    
    
    public function edit(Request $request, $id)
    {
        $listRole = Role::get();
        
        $id = MainSett::decodeId($id);
        $admin = ProfileAdmin::with('user')->findOrFail($id);
        
        return view('backend.master.admin.edit', [
            'list_role' => $listRole,
            'data' => $admin
        ]);
    }
    
    public function update(Request $request, $id)
    {
        $id = MainSett::decodeId($id);
        
        $role = Role::where('id', $request->role)->first();
        
        $profilAdmin = ProfileAdmin::where('id', $id)->first();

        $user = User::where('id', $profilAdmin->user_id)->first();
        $user->full_name = $request->nama_lengkap;
        $user->username = $request->nomor_identitas;
        $user->password = Hash::make('password00');
        $user->role_id = $role->id;
        $user->role_name = $role->name;
        $user->active = 1;
        $user->save();
        
        $profilAdmin->nama_lengkap = $request->nama_lengkap;
        $profilAdmin->nomor_identitas = $request->nomor_identitas;
        $profilAdmin->email = $request->email;
        $profilAdmin->no_hp = $request->no_hp;
        $profilAdmin->active = 1;
        $profilAdmin->save();
        
        return redirect()->route('master-admin.index');
    }
    
    
    public function details(Request $request, $id)
    {
        $admin = ProfileAdmin::findOrFail($id);
        return view('backend.master.m_admin-show', [
            'admin' => $admin
        ]);
    }
    
    
    public function destroy(Request $request, $id)
    {
        $id = MainSett::decodeId($id);
        $admin = ProfileAdmin::where('id', $id)->first();
        
        $user = User::where('id', $admin->user_id)->first();
        $user->delete();
        
        $admin->delete();
        
        return response()->json([
            'type' => 'success',
            'message' => 'delete sukses'
        ], 201);
    }
}
