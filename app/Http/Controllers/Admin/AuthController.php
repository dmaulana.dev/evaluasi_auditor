<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Models\User;
use App\Models\Permission;
use App\Models\PermissionDetails;
use App\Models\ProfileUser;
use Carbon\Carbon;
use Validator;
use Session;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }

    public function register()
    {
        return view('auth.registrasi');
    }

    public function registrasi(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        
        // SIMPAN USER
        $userCreate = new User();
        $userCreate->username = $request->nip;
        $userCreate->password = Hash::make($request->password);
        $userCreate->full_name = $request->fullname;
        $userCreate->role_id = 4;
        $userCreate->role_name = 'auditor';
        $userCreate->active = 1;
        $userCreate->created_at = $date;
        $userCreate->save();

        //SIMPAN PROFIL
        $profilCreate = new ProfileUser();
        $profilCreate->user_id = $userCreate->id;
        $profilCreate->nama_lengkap = $request->fullname;
        $profilCreate->nomor_identitas = $request->nip;        
        $profilCreate->active = 1;
        $profilCreate->save();

        return response()->json([
            'type' => 'success',
            'title' => 'Berhasil Melakukan Registrasi',
            'message' => 'Silahkan Login',
        ], 200);
    }
    public function login(Request $request)
    {
        $credentials = $request->validate([
            'username' => 'required|exists:users',
            'password' => 'required|min:6'
        ]);

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $token = $user->createToken('token-name')->plainTextToken;

            $user->forceFill([
                'api_token' => $token
            ])->save();

            $permissionDetails = collect(PermissionDetails::where('role_id', $user->role_id)->get())->map(function($item){
                return $item->permission_id;
            });

            $permissions = Permission::with('children')->whereNull('parent_id')->whereIn('id', $permissionDetails)->orderBy('order_number', 'ASC')->get();

            $permission_allowed = $permissions->map(function($permission) use ($user){
                $permission_allowed = collect($user->role->permissions)->where('visible', 1);
                return [
                    '_id' => $permission->id,
                    'name' => $permission->name,
                    'url_name' => $permission->url_name,
                    'icon' => $permission->icon,
                    'children' => $permission->children->map(function($child) use ($user){
                        $permission_allowed = collect($user->role->permissions)->where('visible', 1);
                        if ($permission_allowed->pluck('permission_id')->contains($child->id)) {
                            return [
                                '_id' => $child->id,
                                'name' => $child->name,
                                'url_name' => $child->url_name
                            ];
                        }
                    })
                    ->values()
                ];
            })
            ->values();

            $dataUser = [
                'token' => $token,
                'data' => $user,
                'permissions' => $permission_allowed->toArray(),
            ];

            $request->session()->put('data_user', $dataUser);

            return response()->json([
                'type' => 'success',
                'message' => 'Login successfully!',
                'token' => $token,
                'data' => $user,
                'permissions' => $permission_allowed->toArray(),
                'redirect' => route(Permission::find($user->role->permissions->where('visible', 1)->first()->permission_id)->url_name)
            ], 200);

        } else {
            return response()->json([
                'type' => 'error',
                'message' => 'Please check your username or password!',
                'errors' => [
                    'password' => [
                        'Your password is invalid!'
                    ]
                ]
            ], 422);
        }
    }

    public function logout(Request $request ) {
        Session::flush();
        Auth::logout();

        return redirect()->route('auth.login');
    }
}
